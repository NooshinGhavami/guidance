import SimpleITK as sitk
from utilss import utils
import torch
import torch.nn.functional as F
import numpy as np
import scipy.ndimage as sci
from scipy import ndimage
import xml.etree.ElementTree as ET
import vtk # we need this to create a transformed version of the transducer for each frame
import os
import matplotlib.pyplot as plt
import scipy.interpolate as scii

#####################################################
# Arguments: change this to your preference.
#folder = '/home/ag09/data/iFIND/guidance/references_3D/iFIND00103'
#probe_file = '/home/ag09/data/iFIND/guidance/USProbe.stl'
#tmpFolder='/tmp/virtualTrajectory'
folder = r'D:\iFIND00122'
probe_file = r'D:\iFIND00122\USProbe.stl'
tmpFolder = r'D:\iFIND00122\virtualTrajectory'
#planes = ('HC', 'TV', 'AC', 'FL') # The script expects files called 'HCd.mps', etc, within 'folder' above.
planes = ('HC', 'AC', 'FL') # The script expects files called 'HCd.mps', etc, within 'folder' above.
current_plane = 1 # start with HC
plot_trajectory = False
#list_of_virtual_trajectory_indices = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ) # Make this a longer list to compute more trajectories
list_of_virtual_trajectory_indices = range(0,101) # Make this a longer list to compute more trajectories

# Resolution parameters of the virtual scanned data. These parameters will affect the resolution
# and FoV of the images in polar and cartesian
# Polar parameters:
speedup_rate = 2 # higher number produces more frames in the output
smoothness_value = 90
depth = 120 # in mm
sz = (175, 96, 1)
delta_alpha = 1 # degrees
delta_beta = 10 # degrees (does not matter since we are in 2D)
# Cartesian parameters
# Cartesian parameters are automatically computed, and resolution will be isotropic with spacing of 1
# Further customization can be made by creating an info structure and passing it to utils.Polar_to_Cartesian

#######################################################

def read_stl_mesh(filename):
    reader = vtk.vtkSTLReader()
    reader.SetFileName(filename)
    reader.Update()
    return reader.GetOutput()


def write_stl_mesh(mesh, filename):
    writer = vtk.vtkSTLWriter()
    writer.SetFileName(filename)
    writer.SetInputData(mesh)
    writer.Update()


def write_polydata(mesh, filename):
    writer = vtk.vtkPolyDataWriter()
    writer.SetFileName(filename)
    writer.SetInputData(mesh)
    writer.Update()


def transform_mesh(mesh, matrix):
    transform = vtk.vtkTransform()
    if isinstance(matrix, torch.Tensor):
        transform.SetMatrix(matrix.numpy().reshape(-1))
    else:
        transform.SetMatrix(matrix.reshape(-1))
    transformer = vtk.vtkTransformPolyDataFilter()
    transformer.SetTransform(transform)
    transformer.SetInputData(mesh)
    transformer.Update()
    return transformer.GetOutput()


def read_mps_file(filename):
    points = []
    tree = ET.parse(filename)
    root = tree.getroot()
    ts = root[1][0] # time series: here are the points
    for child in ts:
        if child.tag == 'point':
            p = [float(child[2].text), float(child[3].text), float(child[4].text)]
            points.append(p)

    points_array = np.asarray(points, dtype=np.float32)
    return points_array


def matrix_from_point_set(pts):
    # estimate matrix from points by fitting a plane
    centroid = np.mean(pts, axis=0)
    d = np.sqrt(np.sum((centroid - pts[0]) ** 2))

    svd = np.linalg.svd(np.transpose(pts - centroid), full_matrices=True, compute_uv=True)
    rot = svd[0]
    # replace the z and y axes
    # disambiguate the direction of the y axis so that it goes from the first point towards the last
    # and the direction of the x axis so that it goes from first to last too.
    first_to_last = pts[-1] - pts[0]

    M = np.eye(4)
    M[:3, 0] = rot[:3, 1]
    M[:3, 1] = rot[:3, 0]
    sign_x = np.sign(np.dot(M[:3, 0], first_to_last))
    sign_y = np.sign(np.dot(M[:3, 1], first_to_last))
    M[:3, 0] *= sign_x
    M[:3, 1] *= sign_y

    M[:3, 2] = np.cross(M[:3, 0], M[:3, 1])
    M[:3, 3] = (centroid - d * M[:3, 1]).transpose()

    return M


def gradientImages(sitkImage):
    """
    Return the normalized gradient images along x, y and z
    :param sitkImage:
    :return:
    """
    image_shrinked = sitk.GetArrayFromImage(sitkImage)
    # To make sure the gradient is good and goes inward, apply some erosions then cummulative dilations
    for i in range(5):
        image_shrinked = sci.binary_erosion(image_shrinked,  structure=np.ones((3, 3, 3))).astype(image_shrinked.dtype)

    for i in range(10):
        image_shrinked += sci.binary_dilation(image_shrinked,  structure=np.ones((3, 3, 3))).astype(image_shrinked.dtype)

    # Now we have an image that fades gradually in the direction normal to the surface. Compute the gradients.
    # Swap x and z to be consistent with sitk convention
    gz = ndimage.sobel(image_shrinked.astype(np.float32), axis=0, mode='constant')
    gy = ndimage.sobel(image_shrinked.astype(np.float32), axis=1, mode='constant')
    gx = ndimage.sobel(image_shrinked.astype(np.float32), axis=2, mode='constant')

    # normalise
    norm = np.sqrt(gx**2 + gy**2 + gz**2)
    norm[norm==0] = 1
    gx /= norm
    gy /= norm
    gz /= norm

    gx_image = sitk.GetImageFromArray(gx)
    gx_image.SetSpacing(sitkImage.GetSpacing())
    gx_image.SetOrigin(sitkImage.GetOrigin())
    gy_image = sitk.GetImageFromArray(gy)
    gy_image.SetSpacing(sitkImage.GetSpacing())
    gy_image.SetOrigin(sitkImage.GetOrigin())
    gz_image = sitk.GetImageFromArray(gz)
    gz_image.SetSpacing(sitkImage.GetSpacing())
    gz_image.SetOrigin(sitkImage.GetOrigin())

    return gx_image, gy_image, gz_image


def precomputeSurfaceWavePropagationImages(binaryImage, targetPnO, tmpFolder):
    # get the outer shell
    shell_array = sitk.GetArrayFromImage(binaryImage)
    shell_array = shell_array - sci.binary_erosion(shell_array).astype(shell_array.dtype)
    shell = sitk.GetImageFromArray(shell_array)
    shell.SetOrigin(binaryImage.GetOrigin())
    shell.SetSpacing(binaryImage.GetSpacing())
    #sitk.WriteImage(shell, '{}/shell.mhd'.format(tmpFolder))

    # Create a binary image which is 1 only at the point of target.
    map_to_start_point_image = sitk.Image(shell.GetSize(), sitk.sitkInt16)
    map_to_start_point_image.SetSpacing(shell.GetSpacing())
    map_to_start_point_image.SetOrigin(shell.GetOrigin())
    targetP = targetPnO[:3, 3:4]  # P&O of the transducer at target location

    nonzero_indices = np.nonzero(shell_array)
    nonzero_coordinates = [shell.TransformContinuousIndexToPhysicalPoint((float(a), float(b), float(c))) for a, b, c in zip(nonzero_indices[2], nonzero_indices[1], nonzero_indices[0])]
    nonzero_coordinates = np.array(nonzero_coordinates)
    d = np.sum((np.transpose(nonzero_coordinates) - targetP)**2, axis=0)
    id = np.argmin(d)
    distance_to_projection = np.sqrt(d[id])
    targetPp = nonzero_coordinates[id,:]

    targetP_index = np.round(map_to_start_point_image.TransformPhysicalPointToContinuousIndex(targetPp)).astype(np.int)
    map_to_start_point_image[ [int(i) for i in targetP_index] ] = 1
    map_to_start_point = sitk.GetArrayFromImage(map_to_start_point_image)
    lookup_startpoint = map_to_start_point * 0

    # todo: now start going backwards by dilation until the region does not grow any more.
    not_finished = True
    tmp_filenames = []
    i = 0
    struct = sci.generate_binary_structure(3, 2)
    while not_finished == True:
        map_to_start_point_dilated = sci.binary_dilation(map_to_start_point, structure=struct).astype(map_to_start_point.dtype) * shell_array
        not_finished = np.sum(map_to_start_point_dilated-map_to_start_point) > 0
        lookup_startpoint[(map_to_start_point_dilated - map_to_start_point) == 1] = i
        map_to_start_point = map_to_start_point_dilated
        # write to temporary file for later lookup
        mapToStartPoint_image = sitk.GetImageFromArray(map_to_start_point)
        mapToStartPoint_image.SetOrigin(binaryImage.GetOrigin())
        mapToStartPoint_image.SetSpacing(binaryImage.GetSpacing())
        tmp_filenames.append('{}/map_to_start_point_{:03d}.mhd'.format(tmpFolder, i))
        sitk.WriteImage(mapToStartPoint_image, tmp_filenames[-1])
        #
        lookup_startpoint_image = sitk.GetImageFromArray(lookup_startpoint)
        lookup_startpoint_image.SetOrigin(binaryImage.GetOrigin())
        lookup_startpoint_image.SetSpacing(binaryImage.GetSpacing())
        fn2 = '{}/lookup_startpoint_{:03d}.mhd'.format(tmpFolder, i)
        sitk.WriteImage(lookup_startpoint_image, fn2)


        i += 1
    #sitk.WriteImage(shell, '{}/shell.mhd'.format(tmpFolder))
    return shell, shell_array, tmp_filenames, lookup_startpoint, targetPp, distance_to_projection


def nonzero_positions(sitkImage):
    shell_array = sitk.GetArrayFromImage(sitkImage)
    nonzero_indices = np.nonzero(shell_array)
    nonzero_coordinates = [shell.TransformContinuousIndexToPhysicalPoint((float(a), float(b), float(c))) for a, b, c in
                           zip(nonzero_indices[2], nonzero_indices[1], nonzero_indices[0])]
    nonzero_coordinates = np.array(nonzero_coordinates)
    return nonzero_coordinates, nonzero_indices

def randomOriginPosition(sitkImage):
    [nonzero_coordinates, nonzero_indices] = nonzero_positions(sitkImage)
    # randomly pick one
    rand_idx = np.random.randint(0, nonzero_coordinates.shape[0])
    originP = nonzero_coordinates[rand_idx,:]
    originP_idx = (nonzero_indices[0][rand_idx], nonzero_indices[1][rand_idx], nonzero_indices[2][rand_idx]) # do not invert -valid for np arrays
    return originP, originP_idx, rand_idx

def smoothen_trajectory(trajectory, z_vecs, y_vecs, speedup_rate = -1, smoothness = 10):
    """
    Smoothen the trajectory and the vectors

    # fit a fitting spline to the trajectory points, and then estimate the end point of the vectors which
    # in turn will define two more trajectories. Smooth those too, recompute vectors, normalize, return.
    :param trajectory:
    :param z_vecs:
    :param y_vecs:
    :return:
    """

    # precompute trajectories form vector tips
    traj_z = trajectory + z_vecs
    traj_y = trajectory + y_vecs

    # interpolation parameter
    u = np.arange(trajectory.shape[1])

    s = smoothness * len(u)  # smoothing factor
    w = np.ones_like(u)  # weighting factor, to ensure that it fits the edges
    w[0], w[-1] = 100, 100
    # smooth trajectory
    tx = scii.UnivariateSpline(u, trajectory[0, :], s=s, w=w)
    ty = scii.UnivariateSpline(u, trajectory[1, :], s=s, w=w)
    tz = scii.UnivariateSpline(u, trajectory[2, :], s=s, w=w)
    # smooth traj_y and traj_z
    tyx = scii.UnivariateSpline(u, traj_y[0, :], s=s, w=w)
    tyy = scii.UnivariateSpline(u, traj_y[1, :], s=s, w=w)
    tyz = scii.UnivariateSpline(u, traj_y[2, :], s=s, w=w)
    tzx = scii.UnivariateSpline(u, traj_z[0, :], s=s, w=w)
    tzy = scii.UnivariateSpline(u, traj_z[1, :], s=s, w=w)
    tzz = scii.UnivariateSpline(u, traj_z[2, :], s=s, w=w)

    if speedup_rate == 1:
        uo = u
    else:
        uo = np.arange(len(u) * np.round(speedup_rate)) / np.round(speedup_rate)

    txnew = tx(uo)
    tynew = ty(uo)
    tznew = tz(uo)

    tyxnew = tyx(uo)
    tyynew = tyy(uo)
    tyznew = tyz(uo)

    tzxnew = tzx(uo)
    tzynew = tzy(uo)
    tzznew = tzz(uo)

    trajectory_smoothed = np.stack((txnew, tynew, tznew), axis=0)
    traj_y_smoothed = np.stack((tyxnew, tyynew, tyznew), axis=0)
    y_vecs_smoothed = traj_y_smoothed - trajectory_smoothed
    y_vecs_smoothed /= np.sqrt(np.sum(y_vecs_smoothed ** 2, axis=0))
    traj_z_smoothed = np.stack((tzxnew, tzynew, tzznew), axis=0)
    z_vecs_smoothed = traj_z_smoothed - trajectory_smoothed
    z_vecs_smoothed /= np.sqrt(np.sum(z_vecs_smoothed ** 2, axis=0))

    return (trajectory_smoothed, z_vecs_smoothed, y_vecs_smoothed)


#########################################################
# Here starts the actual script
#########################################################


os.makedirs(tmpFolder, exist_ok=True)

# read the image to be sampled
#ref = sitk.ReadImage('{}/registration/iFIND00103_eFoV_ds4.mhd'.format(folder))
ref = sitk.ReadImage('{}/iFIND00122_eFoV.mhd'.format(folder))
probe = read_stl_mesh(probe_file)

# 1 Load the standard planes, in mps format from MITK
# For this to work, the points should be picked roughly along a line from the transducer to the deep end,
# runnign along the image middle, with the first point being the transducer position, and with the last
# point being towards the positive x.

matrices = []
for i, plane in enumerate(planes):
    pts = read_mps_file('{}/{}.mps'.format(folder, plane))
    M = matrix_from_point_set(pts)
    matrices.append(M)
#pts = read_mps_file('{}/{}.mps'.format(folder, planes))
#M = matrix_from_point_set(pts)
#matrices.append(M)

# Based on the ref image, compute the surface along which trajectories are feasible.
# Extract the "shell"
shell = ref > 0

# Compute gradient images
[gx, gy, gz] = gradientImages(shell)

#sitk.WriteImage(gx, '{}/gx.mhd'.format(tmpFolder))
#sitk.WriteImage(gy, '{}/gy.mhd'.format(tmpFolder))
#sitk.WriteImage(gz, '{}/gz.mhd'.format(tmpFolder))

# precompute the wave propagation images for this target
targetPnO = matrices[current_plane] # P&O of the transducer at target location

[shell, shell_array, wave_propagation_image_filenames, lookup_startpoint_image, targetP_projected, distance_to_projection] = precomputeSurfaceWavePropagationImages(shell, targetPnO, tmpFolder)

# ---> Here you would do the loop <----

for virtual_trajectory_no in list_of_virtual_trajectory_indices:
    # todo Now from this pre-computed, in a loop generate the random start point and from there compute the trajectory.
    # find the j-th random origin point
    [originP, originP_idx, originP_idx_1D] = randomOriginPosition(shell)
    n_frame = lookup_startpoint_image[originP_idx]

    # starting from the origin, find in the next image the closest nonzer, and so on.
    trajectory_on_surface = [originP, ]
    origin_index = np.round(shell.TransformPhysicalPointToContinuousIndex(originP)).astype(np.int)
    normals_trajectory = [np.array( (gx[tuple(origin_index.tolist())], gy[tuple(origin_index.tolist())], gz[tuple(origin_index.tolist())]) ) ,]
    trajectory = [originP - normals_trajectory[-1] *distance_to_projection]
    for i in range(n_frame+1):
        im = sitk.ReadImage(wave_propagation_image_filenames[n_frame-i-1])
        nonzeros_image = im > 0.5
        [nonzero_coordinates, nonzero_indices] = nonzero_positions(nonzeros_image)
        # get closest point
        dist = np.sum((nonzero_coordinates - trajectory_on_surface[-1])**2, axis=1)
        closest_idx = np.argmin(dist)

        trajectory_on_surface.append(nonzero_coordinates[closest_idx,:])

        # Append also the normal vector.
        current_index = np.round(shell.TransformPhysicalPointToContinuousIndex(trajectory_on_surface[-1])).astype(np.int)
        current_normal = np.array((gx[tuple(current_index.tolist())], gy[tuple(current_index.tolist())], gz[tuple(current_index.tolist())]))
        normals_trajectory.append(current_normal)
        # move the transducer away against the normal the projected distance

        trajectory.append(trajectory_on_surface[-1] -normals_trajectory[-1] *distance_to_projection)

    trajectory_on_surface.append(targetP_projected)
    trajectory.append(targetPnO[:3,3]) # add the target point as the end of the trajectory
    normals_trajectory.append(targetPnO[:3,1]) # add the y target vector too
    trajectory = np.stack(trajectory, axis=1)
    trajectory_on_surface = np.stack(trajectory_on_surface, axis=1)
    normals_trajectory = np.stack(normals_trajectory, axis=1)

    # Compute the x, y, z axes of the transducer at each stage. I take start and end as fixed and only compute the intermediuate vectors
    z_vecs1 = trajectory_on_surface[:,1:-1]-trajectory_on_surface[:,:-2]
    z_vecs2 = trajectory_on_surface[:,2:]-trajectory_on_surface[:,1:-1]
    z_vecs = (z_vecs1+z_vecs2)/2
    z_vecs  = np.concatenate((trajectory_on_surface[:,0:1],z_vecs,trajectory_on_surface[:,-1:]), axis=1) # add the fixed start and end vectors
    z_vecs /= np.sqrt(np.sum(z_vecs**2, axis=0))
    y_vecs = normals_trajectory # account for start and end point
    y_vecs /= np.sqrt(np.sum(y_vecs**2, axis=0))

    [trajectory, z_vecs, y_vecs] = smoothen_trajectory(trajectory, z_vecs, y_vecs, speedup_rate=speedup_rate, smoothness= smoothness_value)

    x_vecs = np.transpose(np.cross(np.transpose(y_vecs), np.transpose(z_vecs)))
    x_vecs /= np.sqrt(np.sum(x_vecs**2, axis=0))
    z_vecs = np.transpose(np.cross(np.transpose(x_vecs), np.transpose(y_vecs)))



    # Build the matrices
    matrices_trajectory = []
    for i in range(z_vecs.shape[1]):
        M = np.eye(4)
        M[:3, 0] = x_vecs[:, i]
        M[:3, 1] = y_vecs[:, i]
        M[:3, 2] = z_vecs[:, i]
        M[:3, 3] = trajectory[:, i]
        matrices_trajectory.append(M)
    matrices_trajectory.append(targetPnO) # I should not need this.

    if plot_trajectory == True:
        # plot trajectory
        nonzeros_image = im > 0.5
        targetP = targetPnO[:3, 3:4]

        [pos, _] = nonzero_positions(shell)
        fig = plt.figure(figsize=plt.figaspect(0.5)*1.5)
        ax = fig.add_subplot(111, projection='3d')
        # outer shell
        #ax.scatter(pos[:,0], pos[:,1], pos[:,2], marker='.')
        # start and end points
        ax.scatter(originP[0], originP[1], originP[2], marker='x', label='Start', color="r")
        ax.scatter(targetP_projected[0], targetP_projected[1], targetP_projected[2], marker='x', label='End', color="orange")
        # trajectory
        ax.plot(trajectory[0,:], trajectory[1,:], trajectory[2,:], label='Trajectory', color="black")
        # vectors
        ax.quiver(trajectory[0,:-1], trajectory[1,:-1], trajectory[2,:-1], x_vecs[0,:], x_vecs[1,:], x_vecs[2,:], label='x', color='red') #, length=0.1, normalize=True) #depth
        ax.quiver(trajectory[0,:-1], trajectory[1,:-1], trajectory[2,:-1], y_vecs[0,:], y_vecs[1,:], y_vecs[2,:], label='y', color='green') #, length=0.1, normalize=True) #depth
        ax.quiver(trajectory[0,:-1], trajectory[1,:-1], trajectory[2,:-1], z_vecs[0,:], z_vecs[1,:], z_vecs[2,:], label='z', color='blue') #, length=0.1, normalize=True) #depth
        ax.set_xlabel('X Label')
        ax.set_ylabel('Y Label')
        ax.set_zlabel('Z Label')
        #ax.set_aspect('equal', 'box')
        plt.legend()
        plt.show()

    #---------------------------------------

    # todo:
    # 5. Now that you have the path as a collection of voxels, I would fit a paramteric curve to the path
    # to define the transducer positions. You can for example fit a B-spline to the x, y and z components
    # of the points in the trajectory.

    # 6. Now you have the succession of transducer positions you need the orientations. You need to define
    # the orientation of the slicing plane at point A, at point B, and at the target plane (which is guiven
    # by the matrix above). Then you should interpolate these three vlaues over the trajectory; you can't
    # really interpolate matrices so you may need to convert to quaternions first.


    # 7. Now you should have a succession of matrices that pass by the target plane. Just apply these matrices
    # in the loop as below and you have your trajectory.


    ref = sitk.ReadImage('{}/iFIND00122_eFoV.mhd'.format(folder))
    ref_torch, info_ref = utils.sitkToTorch(ref)
    #ref_torch, info_ref = sitkToTorch(ref)
    ref_torch = ref_torch.type(torch.float).unsqueeze(0).unsqueeze(0)


    points = vtk.vtkPoints()

    output_folder = '{}/virtual_trajectory_{}'.format(folder, virtual_trajectory_no)
    os.makedirs(output_folder, exist_ok=True)

    for i in range(len(matrices_trajectory)):

        # define (with a matrix) the sampling point
        #M_filename = '{}/registration/registration_transform_{}.mat'.format(folder, i)
        #M = utils.read_gitk_matrix(M_filename)

        M = torch.from_numpy(matrices_trajectory[i]).type(torch.float32)

        # create the transformed transducer and the trajectory polyline
        points.InsertNextPoint(M[0, 3], M[1, 3], M[2, 3])
        polyline = vtk.vtkPolyLineSource()
        polyline.ClosedOff()
        polyline.SetPoints(points)
        polyline.Update()
        #write_polydata(polyline.GetOutput(), '{}/iFIND00103_compounded_sample_cartesian_trajectory_{:04d}.vtk'.format(output_folder, i))

        transformedProbe = transform_mesh(probe, M)
        #write_stl_mesh(transformedProbe, '{}/iFIND00103_compounded_sample_cartesian_probe_{:04d}.stl'.format(output_folder, i))

        M.unsqueeze_(0)

        # define the spherical coordinates sampling
        sp = (depth/sz[0], delta_alpha*np.pi/180., delta_beta*np.pi/180.)
        ori = [-(sz_-1)/2.0 *sp_ for sz_, sp_ in zip(sz, sp)]
        ori[0] = 0.0
        info_sample = {'size':sz, 'spacing': sp, 'origin': ori}

        sampling_grid = utils.affine_grid_cartesian2polar(M.inverse(), info_sample, info_ref, align_corners=True)
        #sampling_grid = affine_grid_cartesian2polar(M.inverse(), info_sample, info_ref, align_corners=True)
        polar = F.grid_sample(ref_torch, sampling_grid, align_corners=True)

        # convert to itk

        polar_sitk = utils.torchToSitk(polar[0,...].squeeze(0), info_sample)
        #polar_sitk = torchToSitk(polar[0,...].squeeze(0), info_sample)
        #sitk.WriteImage(polar_sitk, '{}/iFIND00103_compounded_sampled_spherical_{:04d}.mhd'.format(output_folder, i))

        cartesian_sitk = utils.Polar_to_Cartesian(polar_sitk)
        #cartesian_sitk = Polar_to_Cartesian(polar_sitk)

        sitk.WriteImage(cartesian_sitk, '{}/iFIND00122_compounded_sample_cartesian_{:04d}.mhd'.format(output_folder, i))

        print('Virtual trajectory {}, frame {:04d}'.format(virtual_trajectory_no, i))
