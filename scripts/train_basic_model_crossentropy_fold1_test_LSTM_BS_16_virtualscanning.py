from datasets import guidancedataset as gdataset
from torchvision import transforms as torchtransforms
from models.regressors import ConvNet, CNN_LSTM
import torch
import torch.nn as nn
import torch.optim as optim
import os
import matplotlib.pyplot as plt
import numpy as np

params = {}
params['BATCH_SIZE'] = 16
params['MAX_EPOCHS'] = 200
params['LR'] = 0.0001
params['do_augmentation'] = True
params['output_folder'] = r'C:\Users\ng20\Desktop\Guidance_Work\Nooshin_find2\Python_Codes\New_Fresh_Code\CNN_9Patients\5_fold_Results_Classification\Fold1\Virtual_Scanning\LSTM\BS16'
params['kernels'] = (3,3,3)
params['strides'] = (3,3,3)
params['channels'] = (16,32,64)
params['fc_features'] = () # leave empty since we're only using one layer so will take output of the last conv layer anyway as input and output features will be just one (single number)

params['sequence_length'] = 20
params['inter_frame_distance'] = 3
params['random_direction_flip'] = True

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

if params['do_augmentation'] == True:
    transform = torchtransforms.Compose([#torchtransforms.ToPILImage(),
                                     #torchtransforms.RandomHorizontalFlip(),
                                     #torchtransforms.RandomAffine(degrees=20.0, translate=(0.75, 0.75), scale=(0.5, 1.5)),
                                     torchtransforms.ToTensor()
                                     ])
else:
    transform = torchtransforms.Compose([torchtransforms.ToTensor()])

validation_transforms = torchtransforms.Compose([torchtransforms.ToTensor()])


trainset = gdataset.guidancedataset_direction(datafile =r'C:\Users\ng20\Desktop\Guidance_Work\Nooshin_find2\Python_Codes\New_Fresh_Code\CNN_9Patients\training_virtualscanning_fold1_abdomen_9patients.txt', transform= transform, preload = True,
                                              sequence_length=params['sequence_length'], inter_frame_distance=params['inter_frame_distance'], random_direction_flip=params['random_direction_flip'], learning_rate=params['LR'])
validationset = gdataset.guidancedataset_direction(datafile =r'C:\Users\ng20\Desktop\Guidance_Work\Nooshin_find2\Python_Codes\New_Fresh_Code\CNN_9Patients\validation_virtualscanning_fold1_abdomen_9patients.txt', transform= validation_transforms, preload = True,
                sequence_length = params['sequence_length'], inter_frame_distance = params['inter_frame_distance'], random_direction_flip=params['random_direction_flip'], learning_rate=params['LR'])

sample = trainset[0]
trainloader = torch.utils.data.DataLoader(trainset, batch_size=params['BATCH_SIZE'], shuffle=False)
validationloader = torch.utils.data.DataLoader(validationset, batch_size=params['BATCH_SIZE'], shuffle=False)


model = CNN_LSTM(sample[0].shape, kernels=params['kernels'], strides=params['strides'], channels=params['channels'], fc_features = params['fc_features']).to(device)
#model = Combine()
model = model.double()

print(model)

#criterion = nn.SmoothL1Loss() # a.k.a HUBER loss: criterion that uses a squared term if the absolute element-wise error falls below 1 and an L1 term otherwise. It is less sensitive than the previous MSE loss for large values
#criterion = nn.MSELoss() # a.k.a HUBER loss: criterion that uses a squared term if the absolute element-wise error falls below 1 and an L1 term otherwise. It is less sensitive than the previous MSE loss for large values
criterion = nn.CrossEntropyLoss() # need to use the cross-entropy loss because now it is more a classification problem rather than regression
#criterion = nn.L1Loss()
optimizer = optim.Adam(model.parameters(), lr=params['LR'],weight_decay=1e-3) # optimiser, the weight decay adds a a regularisation term to the optimiser


losses = []
validation_losses = []
for epoch in range(params['MAX_EPOCHS']):
    running_loss = 0
    correct_training=0
    total_training=0
    model.train()
    for i, (inputs, labels, index) in enumerate(trainloader):

        inputs = inputs.unsqueeze(2) # Add an extra dimension along the third axis to represent the channels
        inputs = inputs.to(device).double()
        #inputs=inputs.double()
        outputs = model(inputs)  # passes a batch of images to the model in the forward pass
        outputs = outputs.to(device)
        labels=labels.type(torch.LongTensor)
        labels = labels.to(device)
        loss = criterion(outputs.float(), torch.max(labels, 1)[0])  # computes the mean squared error between outputs and true labels
        running_loss += loss.item()  # each time the loss is appended to a list so that at the end it can be plotted
        _, predicted = torch.max(outputs.data, 1)  # find the maximum value from the output
        total_training += labels.size(0)  # total image/label size
        correct_training += (predicted == torch.max(labels, 1)[0]).sum().item()  # compute how many correct predictions there are

        # Backpropagation and perform Adam optimisation
        optimizer.zero_grad()  # start by setting the gradients to zero
        loss.backward()  # this step performs the backpropagation on the calculated loss to find the gradients
        optimizer.step()  # Adam optimiser is called
    running_loss /= len(trainloader)
    losses.append(running_loss)

    model.eval()
    correct=0
    total=0
    with torch.no_grad():
        running_validation_loss = 0
        for i, (inputs, labels,index) in enumerate(validationloader):
            inputs = inputs.unsqueeze(2)  # Add an extra dimension along the third axis to represent the channels
            inputs = inputs.to(device).double()
            #inputs=inputs.double()
            outputs = model(inputs)  # passes a batch of images to the model in the forward pass
            outputs = outputs.to(device)
            labels = labels.type(torch.LongTensor)
            labels = labels.to(device)
            loss = criterion(outputs, torch.max(labels, 1)[0])  # computes the mean squared error between outputs and true labels
            running_validation_loss += loss.item()  # each time the loss is appended to a list so that at the end it can be plotted
            _, predicted = torch.max(outputs.data, 1)  # find the maximum value from the output
            total += labels.size(0)  # total image/label size
            correct += (predicted == torch.max(labels, 1)[0]).sum().item()  # compute how many correct predictions there are


        running_validation_loss /= len(validationloader)
        validation_losses.append(running_validation_loss)
        print('[{}] T: {:.6f} | V: {:.6f}'.format(epoch, running_loss, running_validation_loss))

    print('Accuracy of the network on the 13 training images: %d %%' % (100 * correct_training / total_training))

    print('Accuracy of the network on the 4 test images: %d %%' % (100 * correct / total))

p1, = plt.plot(losses)
p2, = plt.plot(validation_losses)
plt.xlabel('epochs')
plt.title('Loss')
plt.legend([p1, p2], ['Training loss','Validation loss'])
plt.savefig('{}/losses{}.png'.format(params['output_folder'],trainset.get_name()))
#plt.show()

# Plot the curve fitting
trainset2 = gdataset.guidancedataset_direction(datafile =r'C:\Users\ng20\Desktop\Guidance_Work\Nooshin_find2\Python_Codes\New_Fresh_Code\CNN_9Patients\training_morepatients_fold1.txt', transform= validation_transforms, preload = True,
                                        sequence_length=params['sequence_length'], inter_frame_distance=params['inter_frame_distance'], learning_rate=params['LR'])
validationset2 = gdataset.guidancedataset_direction(datafile =r'C:\Users\ng20\Desktop\Guidance_Work\Nooshin_find2\Python_Codes\New_Fresh_Code\CNN_9Patients\validation_morepatients_fold1.txt', transform= validation_transforms, preload = True,
                                        sequence_length=params['sequence_length'], inter_frame_distance=params['inter_frame_distance'], learning_rate=params['LR'])

model.eval()
with torch.no_grad():
    ground_truth = []
    estimated_distance = []
    estimated_distance_max = []
    for i in range(len(trainset2)):
        (inputs, labels, index) = trainset2[i]
        inputs = torch.unsqueeze(inputs.to(device).double(),0)
        outputs = model(inputs)  # passes a batch of images to the model in the forward pass
        ground_truth.append(labels[0])
        estimated_distance.append(outputs.squeeze().cpu().detach().numpy())
        estimated_distance_max.append(np.max(outputs.squeeze().cpu().detach().numpy()))

    plt.subplot(221)
    p1, = plt.plot(ground_truth)
    p2, = plt.plot(estimated_distance_max)
    plt.xlabel('frames')
    plt.title('Direction - training')
    plt.legend([p1, p2], ['GT', 'Estimated'])
    df1 = [g-e for g, e in zip(ground_truth,estimated_distance_max)]
    plt.subplot(222)
    plt.plot(df1)
    plt.xlabel('frames')
    plt.ylim((-1, 1))
    plt.title('Direction - training (difference)')

    ground_truth = []
    estimated_distance = []
    estimated_distance_max = []
    for i in range(len(validationset2)):
        (inputs, labels, index) = validationset2[i]
        inputs = torch.unsqueeze(inputs.to(device).double(), 0)
        outputs = model(inputs)  # passes a batch of images to the model in the forward pass
        ground_truth.append(labels[0])
        estimated_distance.append(outputs.squeeze().cpu().detach().numpy())
        estimated_distance_max.append(np.max(outputs.squeeze().cpu().detach().numpy()))

    plt.subplot(223)
    plt.plot(ground_truth)
    plt.plot(estimated_distance_max)
    plt.xlabel('frames')
    plt.title('Direction - validation')
    plt.subplot(224)
    df2 = [g - e for g, e in zip(ground_truth, estimated_distance_max)]
    plt.plot(df2)
    plt.xlabel('frames')
    plt.title('Direction - validation (difference)')
    plt.ylim((-1, 1))
    plt.savefig('{}/fitting{}.png'.format(params['output_folder'], trainset.get_name()))
    #plt.show()
