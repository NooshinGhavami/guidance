import torch.nn as nn
import numpy as np
import torch
from torch.nn import functional as F

# Network Architecture. Similar to the paper mentioned above which we are basing this on, we will have both shared
# layers and view-specific layers. However to start with we will have just one network all shared layers, and if this
# isn't working well then, similar to the paper, we can have the view-specific layers too.
# First create the layer definitions
class ConvNet(nn.Module):
    def __init__(self):
        super(ConvNet, self).__init__()
        self.layer1 = nn.Sequential( # Allows creation of sequentially ordered layers
            nn.Conv2d(1, 16, kernel_size=5, stride=1, padding=2), # create set of convolutional filters
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)) # we use stride of 2 because we want to downsample the image
        self.layer2 = nn.Sequential( # This is the second layer now
            nn.Conv2d(16, 32, kernel_size=5, stride=1, padding=2),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.layer3 = nn.Sequential(  # This is the third layer now
            nn.Conv2d(32, 64, kernel_size=5, stride=1, padding=2),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        #self.layer4 = nn.Sequential(  # This is the third layer now
         #   nn.Conv2d(64, 128, kernel_size=5, stride=1, padding=2),
          #  nn.BatchNorm2d(128),
           # nn.ReLU(),
            #nn.MaxPool2d(kernel_size=2, stride=2))
        #self.drop_out = nn.Dropout()
        self.fc1 = nn.Linear(32 * 32 * 64, 32) # The in_features x output_fetaures
        self.dropout1 = nn.Dropout(p=0.5)
        self.fc2 = nn.Linear(32, 1)
        #self.dropout2 = nn.Dropout(p=0.3)
        self._initialise_weights() #call initialisation

    def _initialise_weights(self): # this function will initialise the weights of all the convolutional layers and the batch norms.
        for m in self.modules():
            if isinstance(m, (nn.Conv2d)):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, (nn.BatchNorm2d)):
                nn.init.constant_(m.weight,1)
                nn.init.constant_(m.bias,0)

    # Now we define how the data flows through the layers in the forward pass of the network
    def forward(self, x):
        out = self.layer1(x)  # feed the input to layer1 (also permute the dimensions since it is not correct at the moment)
        out = self.layer2(out)  # feed the output of layer1 to the input of layer2
        out = self.layer3(out)  # feed the output of layer2 to the input of layer3
        #out = self.layer4(out)  # feed the output of layer2 to the input of layer4
        out = out.reshape(out.size(0), -1)  # flatten the data into a 1D vector before feeding into the fully connected (we want the first dimension as the batch size and it to figure out the other dimension)
        out = self.fc1(out)  # finally apply a fully connected layer to this
        out = self.dropout1(out)  # finally apply a fully connected layer to this
        out = self.fc2(out)  # finally apply a fully connected layer to this
        #out = self.dropout2(out)  # finally apply a fully connected layer to this
        return out  # Return this output from the function



class Regressor(nn.Module):

    def __init__(self, input_size, kernels=(7, 5, 5, 5), strides=(5, 3, 3, 3), channels=(4, 8, 16, 32), fc_features = (128, 16)):
        super().__init__()

        # This should be in spherical coordinates
        self.input_size = tuple(input_size[1:])
        self.sequence_length = input_size[0]
        self.kernels = kernels
        self.strides = strides
        self.channels = channels
        self.fc_features = fc_features

        # conpute conv paddings_cov

        layer_size = (self.input_size,)
        for i in range(len(kernels)):
            layer_size += (tuple(int(np.ceil(l/strides[i])) for l in layer_size[i]),)

        padding = ()
        for i in range(len(kernels)):
            padding += (tuple(int(np.floor((l2*strides[i] - l+1*(kernels[i]-1))/2.0)) for l,l2 in zip(layer_size[i], layer_size[i+1])),)

        # output_padding = ()
        # for i in range(len(kernels)):
        #     output_padding += (tuple(l - (l2 - 1) * strides[i] + 2 * p - 1 * (kernels[i] - 1) - 1 for l,l2, p in zip(layer_size[i], layer_size[i+1], padding[i])),)

        channels = (self.sequence_length,) + channels # add input channel

        self.layer_size = layer_size
        print(self.layer_size)

        cnn_layers = []
        for i in range(len(kernels)):
            current_layer = nn.Sequential(
                nn.Conv2d(channels[i], channels[i+1], kernel_size=kernels[i], stride=strides[i], padding=padding[i]),
                nn.BatchNorm2d(channels[i+1]),
                nn.ReLU(),
            )
            cnn_layers.append(current_layer)
        self.cnn = nn.Sequential(*cnn_layers)

        n_features_last_cnn_layer = np.prod(self.layer_size[-1])*channels[-1]
        self.fc_features = (n_features_last_cnn_layer,) + self.fc_features + (1,)
        linear_layers = []
        for i in range(len(self.fc_features)-1):
            if i < len(self.fc_features) - 2:
                nlactivation = nn.ReLU()
            else:
                nlactivation = nn.Sigmoid()
            linear_layer = nn.Sequential(
                nn.Linear(self.fc_features[i], self.fc_features[i+1]),
                nlactivation,
            )
            linear_layers.append(linear_layer)

        self.linear_layers = nn.Sequential(*linear_layers)

    def forward(self, data):
        batch_size = data.shape[0]
        a = self.cnn(data)
        a = a.view(batch_size,-1)
        y = self.linear_layers(a)
        return y

class Swish(nn.Module):
    """
    The factor parameter makes the function more steep. When factor goes to infinity,
    behaviour is identical to ReLU. The factor must be adjusted to the interval of input data.
    if data is normalised in the input...
    """

    def __init__(self, factor=1):
        super().__init__()  # init the base class
        self.factor = factor

    def forward(self, input):
        return input * torch.sigmoid(input * self.factor)



class Regressor_CE(nn.Module): #cross entropy

    def __init__(self, input_size, kernels=(7, 5, 5, 5), strides=(5, 3, 3, 3), channels=(4, 8, 16, 32), fc_features = (128, 16)):
        super().__init__()

        # This should be in spherical coordinates
        self.input_size = tuple(input_size[1:])
        self.sequence_length = input_size[0]
        self.kernels = kernels
        self.strides = strides
        self.channels = channels
        self.fc_features = fc_features

        # conpute conv paddings_cov

        layer_size = (self.input_size,)
        for i in range(len(kernels)):
            layer_size += (tuple(int(np.ceil(l/strides[i])) for l in layer_size[i]),)

        padding = ()
        for i in range(len(kernels)):
            padding += (tuple(int(np.floor((l2*strides[i] - l+1*(kernels[i]-1))/2.0)) for l,l2 in zip(layer_size[i], layer_size[i+1])),)

        # output_padding = ()
        # for i in range(len(kernels)):
        #     output_padding += (tuple(l - (l2 - 1) * strides[i] + 2 * p - 1 * (kernels[i] - 1) - 1 for l,l2, p in zip(layer_size[i], layer_size[i+1], padding[i])),)

        channels = (self.sequence_length,) + channels # add input channel
        #channels = (1,) + channels # add input channel

        self.layer_size = layer_size
        print(self.layer_size)

        cnn_layers = []
        for i in range(len(kernels)):
            current_layer = nn.Sequential(
                nn.Conv2d(channels[i], channels[i+1], kernel_size=kernels[i], stride=strides[i], padding=padding[i]),
                nn.BatchNorm2d(channels[i+1]),
                nn.ReLU(),
                nn.MaxPool2d(2,2)
            )
            cnn_layers.append(current_layer)
        self.cnn = nn.Sequential(*cnn_layers)

        n_features_last_cnn_layer = np.prod(self.layer_size[-1])*channels[-1]
        self.fc_features = (n_features_last_cnn_layer,) + self.fc_features + (1,)

        self.linear_layers = nn.Sequential(
            #nn.Linear(n_features_last_cnn_layer, 2),
            nn.Linear(64, 2),
            nn.Dropout(p=0.5),
            #nn.ReLU(),
            #nn.Linear(16, 2),
            #nn.Sigmoid()
        )



    def forward(self, data):
        batch_size = data.shape[0]
        a = self.cnn(data)
        a = a.view(batch_size,-1)
        y = self.linear_layers(a)

        return y


class Regressor_CE_Swish(nn.Module): #cross entropy

    def __init__(self, input_size, kernels=(7, 5, 5, 5), strides=(5, 3, 3, 3), channels=(4, 8, 16, 32), fc_features = (128, 16)):
        super().__init__()

        # This should be in spherical coordinates
        self.input_size = tuple(input_size[1:])
        self.sequence_length = input_size[0]
        self.kernels = kernels
        self.strides = strides
        self.channels = channels
        self.fc_features = fc_features

        # conpute conv paddings_cov

        layer_size = (self.input_size,)
        for i in range(len(kernels)):
            layer_size += (tuple(int(np.ceil(l/strides[i])) for l in layer_size[i]),)

        padding = ()
        for i in range(len(kernels)):
            padding += (tuple(int(np.floor((l2*strides[i] - l+1*(kernels[i]-1))/2.0)) for l,l2 in zip(layer_size[i], layer_size[i+1])),)

        # output_padding = ()
        # for i in range(len(kernels)):
        #     output_padding += (tuple(l - (l2 - 1) * strides[i] + 2 * p - 1 * (kernels[i] - 1) - 1 for l,l2, p in zip(layer_size[i], layer_size[i+1], padding[i])),)

        channels = (self.sequence_length,) + channels # add input channel
        #channels = (1,) + channels # add input channel

        self.layer_size = layer_size
        print(self.layer_size)

        cnn_layers = []
        for i in range(len(kernels)):
            current_layer = nn.Sequential(
                nn.Conv2d(channels[i], channels[i+1], kernel_size=kernels[i], stride=strides[i], padding=padding[i]),
                nn.BatchNorm2d(channels[i+1]),
                nn.ReLU(),
                #Swish(),
            )
            cnn_layers.append(current_layer)
        self.cnn = nn.Sequential(*cnn_layers)

        n_features_last_cnn_layer = np.prod(self.layer_size[-1])*channels[-1]
        self.fc_features = (n_features_last_cnn_layer,) + self.fc_features + (1,)

        self.linear_layers = nn.Sequential(
            nn.Linear(n_features_last_cnn_layer, 2),
            nn.Dropout(p=0.5),
            #nn.ReLU(),
            #nn.Linear(16, 2),
            #nn.Sigmoid()
        )



    def forward(self, data):
        batch_size = data.shape[0]
        a = self.cnn(data)
        a = a.view(batch_size,-1)
        y = self.linear_layers(a)

        return y

class Regressor_CE_MoreLinear(nn.Module): #cross entropy

    def __init__(self, input_size, kernels=(7, 5, 5, 5), strides=(5, 3, 3, 3), channels=(4, 8, 16, 32), fc_features = (128, 16)):
        super().__init__()

        # This should be in spherical coordinates
        self.input_size = tuple(input_size[1:])
        self.sequence_length = input_size[0]
        self.kernels = kernels
        self.strides = strides
        self.channels = channels
        self.fc_features = fc_features

        # conpute conv paddings_cov

        layer_size = (self.input_size,)
        for i in range(len(kernels)):
            layer_size += (tuple(int(np.ceil(l/strides[i])) for l in layer_size[i]),)

        padding = ()
        for i in range(len(kernels)):
            padding += (tuple(int(np.floor((l2*strides[i] - l+1*(kernels[i]-1))/2.0)) for l,l2 in zip(layer_size[i], layer_size[i+1])),)

        # output_padding = ()
        # for i in range(len(kernels)):
        #     output_padding += (tuple(l - (l2 - 1) * strides[i] + 2 * p - 1 * (kernels[i] - 1) - 1 for l,l2, p in zip(layer_size[i], layer_size[i+1], padding[i])),)

        channels = (self.sequence_length,) + channels # add input channel
        #channels = (1,) + channels # add input channel

        self.layer_size = layer_size
        print(self.layer_size)

        cnn_layers = []
        for i in range(len(kernels)):
            current_layer = nn.Sequential(
                nn.Conv2d(channels[i], channels[i+1], kernel_size=kernels[i], stride=strides[i], padding=padding[i]),
                nn.BatchNorm2d(channels[i+1]),
                nn.ReLU(),
                nn.MaxPool2d(kernel_size=2, stride=2),
            )
            cnn_layers.append(current_layer)
        self.cnn = nn.Sequential(*cnn_layers)

        n_features_last_cnn_layer = np.prod(self.layer_size[-1])*channels[-1]
        self.fc_features = (n_features_last_cnn_layer,) + self.fc_features + (1,)

        self.linear_layers = nn.Sequential(
            #nn.Linear(n_features_last_cnn_layer, 32),
            nn.Linear(64, 32),
            nn.Dropout(p=0.5),
            #nn.ReLU(),
            nn.Linear(32, 2),
            nn.Dropout(p=0.5),
            #nn.Sigmoid()
        )



    def forward(self, data):
        batch_size = data.shape[0]
        a = self.cnn(data)
        a = a.view(batch_size,-1)
        y = self.linear_layers(a)

        return y



class CNN_LSTM(nn.Module): #cross entropy

    def __init__(self, input_size, kernels=(7, 5, 5, 5), strides=(5, 3, 3, 3), channels=(4, 8, 16, 32), fc_features = (128, 16)):
        super().__init__()

        # This should be in spherical coordinates
        self.input_size = tuple(input_size[1:])
        self.sequence_length = input_size[0]
        self.kernels = kernels
        self.strides = strides
        self.channels = channels
        self.fc_features = fc_features

        # conpute conv paddings_cov

        layer_size = (self.input_size,)
        for i in range(len(kernels)):
            layer_size += (tuple(int(np.ceil(l/strides[i])) for l in layer_size[i]),)

        padding = ()
        for i in range(len(kernels)):
            padding += (tuple(int(np.floor((l2*strides[i] - l+1*(kernels[i]-1))/2.0)) for l,l2 in zip(layer_size[i], layer_size[i+1])),)

        # output_padding = ()
        # for i in range(len(kernels)):
        #     output_padding += (tuple(l - (l2 - 1) * strides[i] + 2 * p - 1 * (kernels[i] - 1) - 1 for l,l2, p in zip(layer_size[i], layer_size[i+1], padding[i])),)

        #channels = (self.sequence_length,) + channels # add input channel
        channels = (1,) + channels # add input channel

        self.layer_size = layer_size
        print(self.layer_size)

        cnn_layers = []
        for i in range(len(kernels)):
            current_layer = nn.Sequential(
                nn.Conv2d(channels[i], channels[i+1], kernel_size=kernels[i], stride=strides[i], padding=padding[i]),
                nn.BatchNorm2d(channels[i+1]),
                nn.ReLU(),
            )
            cnn_layers.append(current_layer)
        self.cnn = nn.Sequential(*cnn_layers)

        n_features_last_cnn_layer = np.prod(self.layer_size[-1])*channels[-1]
        self.fc_features = (n_features_last_cnn_layer,) + self.fc_features + (1,)

        self.linear_layers = nn.Sequential(
            nn.Linear(n_features_last_cnn_layer, 64),
            nn.Dropout(p=0.5),
            #nn.ReLU(),
            #nn.Linear(16, 2),
            #nn.Sigmoid()
        )

        self.rnn = nn.LSTM(
            input_size=64,#512,#32,#132, #similar in scale to timesteps
            hidden_size=32,#128,#64, try smaller hidden size
            num_layers=2,
            batch_first=True,
            bidirectional=True)
        self.linear2 = nn.Linear(32*2, 2)


    def forward(self, data):
        batch_size, timesteps, C, H, W = data.size()
        c_in = data.view(batch_size * timesteps, C, H, W)
        a = self.cnn(c_in)
        b = a.view(-1,1600)
        y = self.linear_layers(b)

        timesteps= data.shape[1]
        r_in = y.view(batch_size, timesteps, -1)
        #r_in = a.view(batch_size, timesteps, -1)
        r_out, (h_n, h_c) = self.rnn(r_in)
        r_out2 = self.linear2(r_out[:, -1, :])
        return r_out2


class CNN_LSTM_MNIST(nn.Module): #cross entropy
    def __init__(self, input_dim, hidden_dim, layer_dim, output_dim):
        super(CNN_LSTM_MNIST, self).__init__()
        # Hidden dimensions
        self.hidden_dim = hidden_dim

        # Number of hidden layers
        self.layer_dim = layer_dim

        # Building your LSTM
        # batch_first=True causes input/output tensors to be of shape
        # (batch_dim, seq_dim, feature_dim)
        self.lstm = nn.LSTM(input_dim, hidden_dim, layer_dim, batch_first=True)

        # Readout layer
        self.fc = nn.Linear(hidden_dim, output_dim)

    def forward(self, x):
        # Initialize hidden state with zeros
        h0 = torch.zeros(self.layer_dim, x.size(0), self.hidden_dim).requires_grad_()

        # Initialize cell state
        c0 = torch.zeros(self.layer_dim, x.size(0), self.hidden_dim).requires_grad_()

        # 28 time steps
        # We need to detach as we are doing truncated backpropagation through time (BPTT)
        # If we don't, we'll backprop all the way to the start even after going through another batch
        out, (hn, cn) = self.lstm(x, (h0.detach(), c0.detach()))

        # Index hidden state of last time step
        # out.size() --> 100, 28, 100
        # out[:, -1, :] --> 100, 100 --> just want last time step hidden states!
        out = self.fc(out[:, -1, :])
        # out.size() --> 100, 10
        return out


class Regressor_CE_WeightSharing(nn.Module): #cross entropy

    def __init__(self, input_size, kernels=(7, 5, 5, 5), strides=(5, 3, 3, 3), channels=(4, 8, 16, 32), fc_features = (128, 16)):
        super().__init__()

        # This should be in spherical coordinates
        self.input_size = tuple(input_size[1:])
        self.sequence_length = input_size[0]
        self.kernels = kernels
        self.strides = strides
        self.channels = channels
        self.fc_features = fc_features

        # conpute conv paddings_cov

        layer_size = (self.input_size,)
        for i in range(len(kernels)):
            layer_size += (tuple(int(np.ceil(l/strides[i])) for l in layer_size[i]),)

        padding = ()
        for i in range(len(kernels)):
            padding += (tuple(int(np.floor((l2*strides[i] - l+1*(kernels[i]-1))/2.0)) for l,l2 in zip(layer_size[i], layer_size[i+1])),)

        # output_padding = ()
        # for i in range(len(kernels)):
        #     output_padding += (tuple(l - (l2 - 1) * strides[i] + 2 * p - 1 * (kernels[i] - 1) - 1 for l,l2, p in zip(layer_size[i], layer_size[i+1], padding[i])),)

        #channels = (self.sequence_length,) + channels # add input channel
        channels = (1,) + channels # add input channel

        self.layer_size = layer_size
        print(self.layer_size)

        cnn_layers = []
        for i in range(len(kernels)):
            current_layer = nn.Sequential(
                nn.Conv2d(channels[i], channels[i+1], kernel_size=kernels[i], stride=strides[i], padding=padding[i]),
                #nn.BatchNorm2d(channels[i+1]),
                nn.ReLU(),
            )
            cnn_layers.append(current_layer)
        self.cnn = nn.Sequential(*cnn_layers)

        n_features_last_cnn_layer = np.prod(self.layer_size[-1])*channels[-1]
        self.fc_features = (n_features_last_cnn_layer,) + self.fc_features + (1,)

        self.linear_layers = nn.Sequential(
            nn.Linear(n_features_last_cnn_layer*self.sequence_length, 2),
            #nn.Dropout(p=0.5),
            #nn.ReLU(),
            #nn.Linear(16, 2),
            #nn.Sigmoid()
        )


    def forward(self, data):
        A=[]
        batch_size = data.shape[0]
        #a = self.cnn(data)
        for k in range(self.sequence_length):
            a = self.cnn((data[:,k,:,:]).unsqueeze(1))
            a = a.view(batch_size,-1)
            A.append(a) # compute cnn for each slice seperately and then append all together in a list
        B = torch.cat(A, 1) # concatanate the flattened arrays for each frame together
        y = self.linear_layers(B)
        return y

class Regressor_CE_WeightSharing_MNIST(nn.Module): #cross entropy

    def __init__(self, input_size, kernels=(7, 5, 5, 5), strides=(5, 3, 3, 3), channels=(4, 8, 16, 32), fc_features = (128, 16)):
        super().__init__()

        # This should be in spherical coordinates
        self.input_size = tuple(input_size[1:])
        self.sequence_length = input_size[0]
        self.kernels = kernels
        self.strides = strides
        self.channels = channels
        self.fc_features = fc_features

        # conpute conv paddings_cov

        layer_size = (self.input_size,)
        for i in range(len(kernels)):
            layer_size += (tuple(int(np.ceil(l/strides[i])) for l in layer_size[i]),)

        padding = ()
        for i in range(len(kernels)):
            padding += (tuple(int(np.floor((l2*strides[i] - l+1*(kernels[i]-1))/2.0)) for l,l2 in zip(layer_size[i], layer_size[i+1])),)

        # output_padding = ()
        # for i in range(len(kernels)):
        #     output_padding += (tuple(l - (l2 - 1) * strides[i] + 2 * p - 1 * (kernels[i] - 1) - 1 for l,l2, p in zip(layer_size[i], layer_size[i+1], padding[i])),)

        #channels = (self.sequence_length,) + channels # add input channel
        channels = (1,) + channels # add input channel

        self.layer_size = layer_size
        print(self.layer_size)

        cnn_layers = []
        for i in range(len(kernels)):
            current_layer = nn.Sequential(
                nn.Conv2d(channels[i], channels[i+1], kernel_size=kernels[i], stride=strides[i], padding=padding[i]),
                nn.BatchNorm2d(channels[i+1]),
                nn.ReLU(),
            )
            cnn_layers.append(current_layer)
        self.cnn = nn.Sequential(*cnn_layers)

        n_features_last_cnn_layer = np.prod(self.layer_size[-1])*channels[-1]
        self.fc_features = (n_features_last_cnn_layer,) + self.fc_features + (1,)

        self.linear_layers = nn.Sequential(
            nn.Linear(n_features_last_cnn_layer*self.sequence_length, 10),
            #nn.Dropout(p=0.5),
            #nn.ReLU(),
            #nn.Linear(16, 2),
            #nn.Sigmoid()
        )


    def forward(self, data):
        A=[]
        batch_size = data.shape[0]
        #a = self.cnn(data)
        for k in range(self.sequence_length):
            a = self.cnn((data[:,k,:,:]).unsqueeze(1))
            a = a.view(batch_size,-1)
            A.append(a) # compute cnn for each slice seperately and then append all together in a list
        B = torch.cat(A, 1) # concatanate the flattened arrays for each frame together
        y = self.linear_layers(B)
        return y


class Regressor_CE_MNIST(nn.Module): #cross entropy

    def __init__(self, input_size, kernels=(7, 5, 5, 5), strides=(5, 3, 3, 3), channels=(4, 8, 16, 32), fc_features = (128, 16)):
        super().__init__()

        # This should be in spherical coordinates
        self.input_size = tuple(input_size[1:])
        self.sequence_length = input_size[0]
        self.kernels = kernels
        self.strides = strides
        self.channels = channels
        self.fc_features = fc_features

        # conpute conv paddings_cov

        layer_size = (self.input_size,)
        for i in range(len(kernels)):
            layer_size += (tuple(int(np.ceil(l/strides[i])) for l in layer_size[i]),)

        padding = ()
        for i in range(len(kernels)):
            padding += (tuple(int(np.floor((l2*strides[i] - l+1*(kernels[i]-1))/2.0)) for l,l2 in zip(layer_size[i], layer_size[i+1])),)

        # output_padding = ()
        # for i in range(len(kernels)):
        #     output_padding += (tuple(l - (l2 - 1) * strides[i] + 2 * p - 1 * (kernels[i] - 1) - 1 for l,l2, p in zip(layer_size[i], layer_size[i+1], padding[i])),)

        channels = (self.sequence_length,) + channels # add input channel

        self.layer_size = layer_size
        print(self.layer_size)

        cnn_layers = []
        for i in range(len(kernels)):
            current_layer = nn.Sequential(
                nn.Conv2d(channels[i], channels[i+1], kernel_size=kernels[i], stride=strides[i], padding=padding[i]),
                nn.BatchNorm2d(channels[i+1]),
                nn.ReLU(),
            )
            cnn_layers.append(current_layer)
        self.cnn = nn.Sequential(*cnn_layers)

        n_features_last_cnn_layer = np.prod(self.layer_size[-1])*channels[-1]
        self.fc_features = (n_features_last_cnn_layer,) + self.fc_features + (1,)

        self.linear_layers = nn.Sequential(
            #nn.Linear(n_features_last_cnn_layer, 32),
            nn.Linear(n_features_last_cnn_layer, 10),
            #n.Dropout(p=0.5),
            #nn.ReLU(),
            #nn.Linear(32, 10),
            #nn.Sigmoid()
        )

    def forward(self, data):
        batch_size = data.shape[0]
        a = self.cnn(data)
        a = a.view(batch_size, -1)
        y = self.linear_layers(a)
        return y



