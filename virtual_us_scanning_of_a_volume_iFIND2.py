#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed 5th November 2020

@author: Nooshin
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-


# -*- coding: utf-8 -*-
"""Virtual US scanning of a volume.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1O3bOzIcUSWC4rte278zHcvGx2sHjkO8d

# Summary
How to virtually scan an image in polar/spherical coordinates and convert to cartesian

Given a 3D image in ITK format (so it can be read with Simple ITK), define a view point and an orientation via a 4x4 matrix; 
and define a sampling grid, this is, the grid of the ultrasound image in polar or spherical coordinates. 
Then, sample the reference image using the sampling grid yields a polar (or spherical) image. Last, this can be converted to Cartesian.

The sampling grid defines the extent (depth and angle width) of the ultrasound image and its resolution.

Author: Alberto Gomez (alberto.gomez@kcl.ac.uk)
"""

import SimpleITK as sitk
import torch
import torch.nn.functional as F
import numpy as np
import scipy
import scipy.interpolate
import os
from celluloid import Camera
from matplotlib import pyplot as plt
import csv
import h5py


"""# Auxiliary functions"""

def create_random_image(extent, size):
    origin = [ extent[i][0] for i in range(3)]
    spacing = [ (extent[i][1]-extent[i][0])/(size[i]-1) for i in range(3)]
    data = (np.zeros(list(size))).astype(np.float32)
    data[:int(size[0]/2),:,:]=125
    data[:, int(size[1]/3):int(2*size[1]/3),:]=64
    data[:, :, int(3*size[2]/5):int(4*size[2]/5)]=164
    noise = (np.random.uniform(size=size)*50.0).astype(np.float32)

    data += noise
    
    reference = sitk.GetImageFromArray(data)
    reference.SetOrigin(origin)
    reference.SetSpacing(spacing)
    return reference


def load_sitk(datafile,extent, size):
    origin = [extent[i][0] for i in range(3)]
    spacing = [(extent[i][1] - extent[i][0]) / (size[i] - 1) for i in range(3)]

    reference = sitk.ReadImage(datafile)
    #reference = sitk.GetImageFromArray(images)
    reference.SetOrigin(origin)
    reference.SetSpacing(spacing)
    return reference

def load_dicom_image_as_sitk(dicom_file_dir, extent, size):
    '''
    Read in DICOM folder, rescale to 0-255 and convert to sitk format.

    Parameters
    ----------
    dicom_file_dir : str
        Path to folder containing DICOM study.


    '''
    
    files = []
    for fname in os.listdir(dicom_file_dir):
        files.append(pydicom.dcmread(dicom_file_dir + fname))
        slices = []
        skipcount = 0
        for f in files:
            if hasattr(f, 'SliceLocation'):
                slices.append(f)
            else:
                skipcount = skipcount + 1
    
    slices = sorted(slices, key=lambda s: s.SliceLocation)
    img_shape = list(slices[0].pixel_array.shape)
    img_shape.append(len(slices))
    img3d = np.zeros(img_shape)
    
    # fill 3D array with the images from the files
    for i, s in enumerate(slices):
        img2d = s.pixel_array
        img3d[:, :, i] = img2d
    rescale_images = []  
    for image in img3d:
        image *= 255.0/image.max()
        rescale_images += [image]
    
    img3d = np.array(rescale_images)
    # img3d = np.moveaxis(np.array(img3d), 0, 1)
    img3d = np.moveaxis(np.array(img3d), -1, 0)
    origin = [ extent[i][0] for i in range(3)]
    spacing = [ (extent[i][1]-extent[i][0])/(size[i]-1) for i in range(3)]
    reference = sitk.GetImageFromArray(img3d)
    reference.SetOrigin(origin)
    reference.SetSpacing(spacing)
    return reference

def get_image_pixel_positions(itk_img):
    """
    Get the positions of pixels.
    :param itk_img:
    :return:
    """
    spacing = itk_img.GetSpacing()
    origin = itk_img.GetOrigin()
    size = itk_img.GetSize()

    if itk_img.GetDimension()==3:
        positions = np.mgrid[0.0:size[0],0.0:size[1],0.0:size[2]]
        for i in range(itk_img.GetDimension()):
            positions[i, :, :, :] = positions[i, :, :, :] * spacing[i] + origin[i]
    else:
        positions = np.mgrid[0.0:size[0], 0.0:size[1]]
        for i in range(itk_img.GetDimension()):
            positions[i, :, :] = positions[i, :, :] * spacing[i] + origin[i]

    return positions

def sitkToTorch(sitk_image, transpose = True):
    np_image = sitk.GetArrayFromImage(sitk_image)  # need to permute indices for consistency
    if transpose is True:
        np_image = np.transpose(np_image)
        tensor_info = {'spacing': sitk_image.GetSpacing(), 'origin': sitk_image.GetOrigin(),
                       'size': sitk_image.GetSize()}
    else:
        tensor_info = {'spacing': sitk_image.GetSpacing()[::-1], 'origin': sitk_image.GetOrigin()[::-1],
                       'size': sitk_image.GetSize()[::-1]}

    image_tensor = torch.from_numpy(np_image.astype(np.float32))
    return image_tensor, tensor_info

def torchToSitk(torch_tensor, tensor_info, transpose = True):
    """

    Args:
        torch_tensor: torch tensor (cpu)
        tensor_info: dictionary
        transpose: if data was transposed at load time or not

    Returns:

    """
    if tensor_info['size'][-1] == 1:
        new_info = {'size': tensor_info['size'][:-1], 'spacing': tensor_info['spacing'][:-1], 'origin': tensor_info['origin'][:-1]}
        tensor_info = new_info
        torch_tensor =torch_tensor.squeeze(-1)


    np_image = torch_tensor.numpy()
    if transpose is True:
        np_image = np.transpose(np_image)
        # if data was transposed at load time, then spacing/origin were not
        spacing = tensor_info['spacing']
        origin = tensor_info['origin']
    else:
        spacing = tensor_info['spacing'][::-1]
        origin = tensor_info['origin'][::-1]

    sitk_image = sitk.GetImageFromArray(np_image)

    sitk_image.SetSpacing(spacing)
    sitk_image.SetOrigin(origin)
    return sitk_image

def Polar_to_Cartesian(itk_img_spherical, itk_img_cartesian_ref = None,
                       cartesian_info = None, in_degrees = False):
    """
    Convert to 3D Cartesian coordinates
    :param itk_image:
    :return:
    """

    BEAM_DIMENSION_CARTESIAN = 1  # depth dimension in cartesian
    BEAM_DIMENSION_SPHERICAL = 0  # depth dimension in spherical

    D = itk_img_spherical.GetDimension()
    spherical_spacing = list(itk_img_spherical.GetSpacing())
    spherical_size = list(itk_img_spherical.GetSize())
    spherical_origin = list(itk_img_spherical.GetOrigin())
    if in_degrees is True:
        for i in range(D):
            if i != BEAM_DIMENSION_SPHERICAL:
                spherical_spacing[i] *= np.pi / 180.0
                spherical_origin[i] *= np.pi / 180.0

    if itk_img_cartesian_ref is None:
        # Create my own cartesian ref
        if cartesian_info is None:
            cartesian_info = {'spacing' : None, 'origin': None, 'size': None}
            cartesian_info['spacing'] = list((1.0,) * D)
            cartesian_info['spacing'][BEAM_DIMENSION_CARTESIAN] = spherical_spacing[BEAM_DIMENSION_SPHERICAL]
            length = spherical_size[BEAM_DIMENSION_SPHERICAL] * spherical_spacing[BEAM_DIMENSION_SPHERICAL]
            spherical_bounds = ()
            cartesian_bounds = [-1., ]*D
            cartesian_info['origin'] = [-1., ]*D
            for i in range(D):
                spherical_bounds += ((spherical_origin[i], spherical_origin[i]+(spherical_size[i]-1)*spherical_spacing[i]),)

            cartesian_info['origin'][0] = length * np.sin(spherical_bounds[1][0])
            cartesian_bounds[0] = length * np.sin(spherical_bounds[1][1])
            cartesian_info['origin'][BEAM_DIMENSION_CARTESIAN] = spherical_bounds[BEAM_DIMENSION_SPHERICAL][0]
            cartesian_bounds[BEAM_DIMENSION_CARTESIAN] = spherical_bounds[BEAM_DIMENSION_SPHERICAL][1]
            if D > 2:
                if spherical_size[-1] > 1:
                    cartesian_info['origin'][2] = length * np.sin(spherical_bounds[2][0])
                    cartesian_bounds[2] = length * np.sin(spherical_bounds[2][1])
                else:
                    cartesian_info['origin'][2] = 0.0
                    cartesian_bounds[2] = 0.0

            cartesian_info['size'] = [int(np.ceil((cb-co)/csp+1)) for cb, co, csp in zip(cartesian_bounds, cartesian_info['origin'], cartesian_info['spacing'])]

        itk_img_cartesian_ref = sitk.Image(cartesian_info['size'], itk_img_spherical.GetPixelID())
        itk_img_cartesian_ref.SetOrigin(cartesian_info['origin'])
        itk_img_cartesian_ref.SetSpacing(cartesian_info['spacing'])

    # compute the spherical values of the cartesian coordinates
    cartesian_pos = get_image_pixel_positions(itk_img_cartesian_ref)
    cartesian_pos = torch.tensor(cartesian_pos)

    # prepare the output
    spherical_size = itk_img_spherical.GetSize()

    # compute the sampling locations
    polar_grid = ()
    for i in range(D):
        polar_grid += (np.array(range(spherical_size[i])) * spherical_spacing[i] + spherical_origin[i],)

    cartesian_grid_in_polar = ()
    r = torch.sqrt(torch.sum(cartesian_pos ** 2, dim=0))

    cartesian_grid_in_polar += (r,)  # r
    if D == 2:
        angle_x = torch.asin(cartesian_pos[0, ...] / r)
    else:
        angle_x = torch.atan2(cartesian_pos[0, ...], cartesian_pos[1, ...])
    if in_degrees is True:
        angle_x *= np.pi / 180.
    cartesian_grid_in_polar += (angle_x,)

    if D == 3:
        angle_y = torch.asin(cartesian_pos[2, ...] / r)
        if in_degrees is True:
            angle_y *= np.pi / 180.
        cartesian_grid_in_polar += (angle_y,)

    transpose_order = [i for i in range(D - 1, -1, -1)]
    input_spherical_image_data = np.transpose(sitk.GetArrayFromImage(itk_img_spherical), transpose_order)
    interpolated = scipy.interpolate.interpn(polar_grid, input_spherical_image_data, cartesian_grid_in_polar, method='linear',
                                             bounds_error=False, fill_value=np.nan)

    reshaped = np.transpose(interpolated, transpose_order)
    maxval = np.nanmax(reshaped)
    if maxval <= 1.1:  # observer vals > 1 from torch, maybe some randomness/precission problem. In theory this should be 1
        reshaped *= 255
    reshaped = np.array(reshaped, dtype=np.uint8)

    itk_img_cartesian = sitk.GetImageFromArray(reshaped)
    itk_img_cartesian.SetSpacing(itk_img_cartesian_ref.GetSpacing())
    itk_img_cartesian.SetOrigin(itk_img_cartesian_ref.GetOrigin())

    return itk_img_cartesian

def Cartesian_to_Polar(itk_img_cartesian, itk_img_polar_ref = None,
                       polar_info  = None, in_degrees = False):
    """
    Convert to 2D polar or 3D spherical coordinates
    :param itk_img_cartesian: cartesian image to be converted
    :param itk_img_polar_ref: reference where polar spacing, origin, etc will be borrowed from. If not, one is created.
    # in polar/spherical coordinates, the first dimension (0) is the depth, and the remaining two are the angles (1: lateral , 2 elevation)
    # In cartesian, the depth dimension should be y
    # polar_info is an optional dict with 'spacing', 'origin' and 'size'
    :return: polar converted image
    """

    # Trick needed if 2D images are saved as 3D images
    did_remove_a_dim = False
    if len(itk_img_cartesian.GetSize()) > 2 and itk_img_cartesian.GetSize()[2] == 1:
        newsize = list(itk_img_cartesian.GetSize())
        newsize[2] = 0
        itk_img_cartesian = sitk.Extract(itk_img_cartesian, newsize, (0, 0, 0), 2)
        did_remove_a_dim = True

    D = itk_img_cartesian.GetDimension()
    BEAM_DIMENSION_CARTESIAN = 1 # depth dimension in cartesian
    BEAM_DIMENSION_SPHERICAL = 0  # depth dimension in spherical

    # prepare the output
    cartesian_spacing = itk_img_cartesian.GetSpacing()
    cartesian_origin = itk_img_cartesian.GetOrigin()
    cartesian_size = itk_img_cartesian.GetSize()

    if itk_img_polar_ref is None:
        # Create my own polar ref

        if polar_info is None:
            polar_info ={'spacing': (np.ones(D-1) * np.pi / 180.0).tolist()}

        if len(polar_info['spacing']) < D:
            polar_info['spacing'] = [itk_img_cartesian.GetSpacing()[BEAM_DIMENSION_CARTESIAN]]+ list(polar_info['spacing'])

        MAX_ANGLE = 2.0 * np.pi / 3.0 # +- 72 degrees, should cover comfortably any frustum

        min_width = 10E+10 # crazy large number
        for i in range(D):
            if i != BEAM_DIMENSION_CARTESIAN and cartesian_size[i] < min_width:
                min_width = cartesian_size[i]

        extent = []
        for i in range(D):
            extent.append(cartesian_size[i]/min_width * MAX_ANGLE )

        extended_depth_size = cartesian_size[BEAM_DIMENSION_CARTESIAN]
        extent[BEAM_DIMENSION_SPHERICAL] = (extended_depth_size - 1) * cartesian_spacing[BEAM_DIMENSION_CARTESIAN] #- cartesian_origin[BEAM_DIMENSION_CARTESIAN]

        polar_info['origin'] = [-e/2.0 for e in extent]
        polar_info['origin'][BEAM_DIMENSION_SPHERICAL] = 0
        polar_size = [round(extent[i]/polar_info['spacing'][i])+1 for i in range(D) ]
        if 'size' in polar_info:
            polar_size[0] = polar_info['size'][0]

        polar_info['size'] = polar_size

        itk_img_polar_ref = sitk.Image(polar_info['size'], itk_img_cartesian.GetPixelID())
        itk_img_polar_ref.SetOrigin(polar_info['origin'])
        itk_img_polar_ref.SetSpacing(polar_info['spacing'])
    else:
        if in_degrees is True:
            # ref will be in degrees, but for intermediate computations I have to put it in radians
            ref_spacing = [p * np.pi/180.0 for p in itk_img_polar_ref.GetSpacing()]
            ref_spacing[BEAM_DIMENSION_SPHERICAL] = itk_img_polar_ref.GetSpacing()[BEAM_DIMENSION_SPHERICAL]
            ref_origin = [p * np.pi/180.0 for p in itk_img_polar_ref.GetOrigin()]
            ref_origin[BEAM_DIMENSION_SPHERICAL] = itk_img_polar_ref.GetOrigin()[BEAM_DIMENSION_SPHERICAL]
            itk_img_polar_ref.SetSpacing(ref_spacing)
            itk_img_polar_ref.SetOrigin(ref_origin)

    polar_pos = get_image_pixel_positions(itk_img_polar_ref)

    # compute the sampling locations
    points = ()
    for i in range(D):
        points += (np.array(range(cartesian_size[i])) * cartesian_spacing[i] + cartesian_origin[i],)

    if D == 2:
        x = polar_pos[BEAM_DIMENSION_SPHERICAL] * np.sin(polar_pos[1])
        y = polar_pos[BEAM_DIMENSION_SPHERICAL] * np.cos(polar_pos[1])
        xi = np.column_stack((np.transpose(x).reshape(-1, 1), np.transpose(y).reshape(-1, 1)))
    elif D == 3:
        # y is the axis along which ultrasound beam goes
        x = polar_pos[BEAM_DIMENSION_SPHERICAL] * np.sin(polar_pos[1]) * np.cos(polar_pos[2])
        y = polar_pos[BEAM_DIMENSION_SPHERICAL] * np.cos(polar_pos[1]) * np.cos(polar_pos[2])
        z = polar_pos[BEAM_DIMENSION_SPHERICAL] * np.sin(polar_pos[2])

        xi = np.column_stack((np.transpose(x).reshape(-1, 1), np.transpose(y).reshape(-1, 1), np.transpose(z).reshape(-1, 1)))
    else:
        print("[ERROR] Cartesian_to_Polar: only 2D or 3D supported, dimension was ",D)
        return -1

    transpose_order = [i for i in range(D - 1, -1, -1)]
    input_image_data = np.transpose(sitk.GetArrayFromImage(itk_img_cartesian), transpose_order)

    # sample the  values
    interpolated = scipy.interpolate.interpn(points, input_image_data, xi, method='nearest', bounds_error=False, fill_value=np.nan)
    if D == 2:
        reshaped = interpolated.reshape(itk_img_polar_ref.GetSize()[transpose_order[0]], itk_img_polar_ref.GetSize()[transpose_order[1]])
    else:
        reshaped = interpolated.reshape(itk_img_polar_ref.GetSize()[transpose_order[0]], itk_img_polar_ref.GetSize()[transpose_order[1]], itk_img_polar_ref.GetSize()[transpose_order[2]])

    reshaped = np.array(reshaped, dtype=np.uint8)

    # Now crop the image to remove black edges: dims 1 and 2
    out_origin = list(itk_img_polar_ref.GetOrigin())
    out_spacing = list(itk_img_polar_ref.GetSpacing())
    do_crop = True
    if do_crop:
        if D ==3:
            id_x = np.nonzero(np.sum(np.sum(reshaped, axis=1), axis=1))
        elif D == 2:
            id_x = np.nonzero(np.sum(reshaped, axis=1))
        if len(id_x[0]) > 0:
            if D > 2:
                id_y = np.nonzero(np.sum(np.sum(reshaped, axis=0), axis=1))
                reshaped = reshaped[id_x[0][0]:id_x[0][-1]+1, id_y[0][0]:id_y[0][-1]+1,:]
                # note that coordinates are transposed
                out_origin[2] += out_spacing[2] * id_x[0][0]
                out_origin[1] += out_spacing[1] * id_y[0][0]
            else:
                reshaped = reshaped[id_x[0][0]:id_x[0][-1]+1,:]
                out_origin[1] += out_spacing[1] * id_x[0][0]


    itk_img_polar = sitk.GetImageFromArray(reshaped)
    if in_degrees == True:
        out_spacing = [p * 180.0 / np.pi for p in out_spacing]
        out_spacing[BEAM_DIMENSION_SPHERICAL] = itk_img_polar_ref.GetSpacing()[BEAM_DIMENSION_SPHERICAL]
        out_origin = [p * 180.0 / np.pi for p in out_origin]
        out_origin[BEAM_DIMENSION_SPHERICAL] = itk_img_polar_ref.GetOrigin()[BEAM_DIMENSION_SPHERICAL]
    itk_img_polar.SetSpacing(out_spacing)
    itk_img_polar.SetOrigin(out_origin)
    return itk_img_polar


def affine_grid_cartesian2polar(Ms2t, info_polar_target_grid, info_cartesian_source_grid, align_corners=False, nc = 1, in_degrees = False):
    """
    Generate a grid to sample at locations in a polar target grid from a cartesian source image

    Args:
        Ms2t: transformation matrix, from source 2 target
        align_corners: True or None
        nc: number of channels
        in_degrees: True if angles are in degrees, default in radians

    Returns:
        a normalised polar grid that can be fed to a torch sampler

    """
    BEAM_DIMENSION_CARTESIAN = 1  # depth dimension in cartesian
    BEAM_DIMENSION_SPHERICAL = 0  # depth dimension in spherical

    device = Ms2t.device
    D = Ms2t.shape[-1] - 1
    bs = Ms2t.shape[0]

    # 1-3. Create polar grid in worldcoordinates and transformed
    M_t2tnorm = (matrix_tensor_to_normalized_grid(info_polar_target_grid['origin'], info_polar_target_grid['spacing'],
                                                  info_polar_target_grid['size'])).unsqueeze(0).expand_as(Ms2t).to(device)

    # Initially we create a polar grid with identity transform
    polar_shape_target = [bs, nc] + list(info_polar_target_grid['size'])
    M_t2tnorm_swaped = swap_dimensions(M_t2tnorm.inverse(), 0, D - 1)
    target_grid_polar = F.affine_grid(M_t2tnorm_swaped[:, :-1, :], polar_shape_target, align_corners=align_corners).to(
        device)
    # we need to do a coordinate swap to do the conversion to polar
    target_grid_polar = target_grid_polar.flip(-1)
    # Transform the grid to cartesian
    cartesian_coords = ()
    factor = 1.0
    if in_degrees == True:
        factor = np.pi / 180.
    if D == 2:
        x = target_grid_polar[..., BEAM_DIMENSION_SPHERICAL] * torch.sin(target_grid_polar[..., 1] * factor)
        y = target_grid_polar[..., BEAM_DIMENSION_SPHERICAL] * torch.cos(target_grid_polar[..., 1] * factor)
        cartesian_coords = (x, y)
    elif D == 3:
        # y is the axis along which ultrasound beam goes
        x = target_grid_polar[..., BEAM_DIMENSION_SPHERICAL] * torch.sin(
            target_grid_polar[..., 1] * factor) * torch.cos(target_grid_polar[..., 2] * factor)
        y = target_grid_polar[..., BEAM_DIMENSION_SPHERICAL] * torch.cos(
            target_grid_polar[..., 1] * factor) * torch.cos(target_grid_polar[..., 2] * factor)
        z = target_grid_polar[..., BEAM_DIMENSION_SPHERICAL] * torch.sin(target_grid_polar[..., 2] * factor)
        cartesian_coords = (x, y, z)
    else:
        print(
            '[ERROR] utils.affine_grid_polar2polar : only 2D or 3D images are supported but dimension was {}'.format(D))
    target_grid_cartesian = torch.stack(cartesian_coords, dim=D + 1)

    # Now transform the cartesian grid. No need to swap since it is swaped already
    Mt2s = Ms2t.inverse()
    if D == 2:
        target_grid_cart_tx_to_source_cart = Mt2s[:, :-1, :-1].unsqueeze(1).matmul(
            target_grid_cartesian.permute(0, 1, 3, 2))
        target_grid_cart_tx_to_source_cart = target_grid_cart_tx_to_source_cart.permute(0, 1, 3, 2) + Mt2s[:, :-1,
                                                                                                      -1].unsqueeze(
            1).unsqueeze(2)
    elif D == 3:
        target_grid_cart_tx_to_source_cart = Mt2s[:, :-1, :-1].unsqueeze(1).unsqueeze(2).matmul(
            target_grid_cartesian.permute(0, 1, 2, 4, 3))
        target_grid_cart_tx_to_source_cart = target_grid_cart_tx_to_source_cart.permute(0, 1, 2, 4, 3) + Mt2s[:, :-1,
                                                                                                         -1].unsqueeze(
            1).unsqueeze(2).unsqueeze(3)
    else:
        print(
            '[ERROR] utils.affine_grid_cartesian2polar : only 2D or 3D images are supported but dimension was {}'.format(D))


    # now that polars are converted, flip back again
    target_grid_cart_tx_to_source_cart = target_grid_cart_tx_to_source_cart.flip(-1)
    # 5. Normalize the spherical grid
    M_s2snorm = matrix_tensor_to_normalized_grid(info_cartesian_source_grid['origin'],
                                                 info_cartesian_source_grid['spacing'],
                                                 info_cartesian_source_grid['size']).unsqueeze(0).expand_as(Ms2t).to(device)

    M_s2snorm_swaped = swap_dimensions(M_s2snorm, 0, D - 1)
    if D == 2:
        cartesian_grid_source_normalised = M_s2snorm_swaped[:, :-1, :-1].unsqueeze(1).matmul(
            target_grid_cart_tx_to_source_cart.permute(0, 1, 3, 2))
        cartesian_grid_source_normalised = cartesian_grid_source_normalised.permute(0, 1, 3, 2) + M_s2snorm_swaped[:,
                                                                                                  :-1, -1].unsqueeze(
            1).unsqueeze(2)
    elif D == 3:
        cartesian_grid_source_normalised = M_s2snorm_swaped[:, :-1, :-1].unsqueeze(1).unsqueeze(2).matmul(
            target_grid_cart_tx_to_source_cart.permute(0, 1, 2, 4, 3))
        cartesian_grid_source_normalised = cartesian_grid_source_normalised.permute(0, 1, 2, 4, 3) + M_s2snorm_swaped[:,
                                                                                                     :-1, -1].unsqueeze(
            1).unsqueeze(2).unsqueeze(3)
    else:
        print(
            '[ERROR] utils.affine_grid_polar2polar : only 2D or 3D images are supported but dimension was {}'.format(D))


    return cartesian_grid_source_normalised

def matrix_tensor_to_normalized_grid(ori_, spc_, sz_):
    """ Compute the image to normalised grid matrix adapted to a
    normalised torch grid (defined in normalised coordinates between -1
    and 1 that stretch the entire grid size), when the original matrix M
    is defined in world coordinates of an image with parameters ori_,
    spc_ and sz_

    Args:
        ori_: origin (coordinates of pixel of index 0)
        spc_: spacing
        sz_: size

    Returns:

    """
    D = len(ori_)

    ori = torch.tensor(ori_)
    spc = torch.tensor(spc_)
    sz = torch.tensor(sz_).type(spc.type())

    # Create a matrix to go from the image coordinates
    # to the normalised coordinates
    M_im2norm = torch.eye(D + 1)
    # There will be no rotation. Scaling is determined by spacing
    spacing_of_image = spc
    sp_of_normalised_grid = 2 / (sz - 1)  # extent is 2 because it goes from -1 to +1
    if sz[-1] == 1:
        # really a 2D image
        sp_of_normalised_grid[-1] = spacing_of_image[-1]

    scale_im2norm = sp_of_normalised_grid / spacing_of_image
    M_im2norm[:-1, :-1] = torch.eye(D) * scale_im2norm
    # There wil be an offset for differences in origin.  normalised origin is -1, -1
    # correspongding to a (0,0) world coordinate at the centre of the image.
    ori_of_normalised_grid = -(sz - 1) * sp_of_normalised_grid / 2  # i.e. -1
    ori_of_image_after_scaling = ori * scale_im2norm
    M_im2norm[:-1, D] = ori_of_normalised_grid - ori_of_image_after_scaling

    return M_im2norm

def swap_dimensions(Ms2t, d0, d1):
    # swap rows
    Ms2t_r  = torch.clone(Ms2t)
    Ms2t_r[:, d0,:] = Ms2t[:, d1,:]
    Ms2t_r[:, d1, :] = Ms2t[:, d0, :]
    # swap columns
    Ms2t_c = torch.clone(Ms2t_r)
    Ms2t_c[:, :, d0] = Ms2t_r[:, :, d1]
    Ms2t_c[:, :, d1] = Ms2t_r[:, :, d0]
    return Ms2t_c



def perform_ordered_transform(view_point, view_orientation, theta, rotation_order):
    '''
    Performs a rotation about cartesian coordinate axes by an input angle in a specified order.

    Parameters
    ----------
    view_point : Torch tensor 
        Coordinates which the view point is taken as.
    view_orientation : Torch tensor 
        Matrix of view orientation.
    theta : List
        [1x3] vector of angles for rotation corresponding to x-y-z axes in degrees.
    rotation_order : str
        Order in which to perform rotation e.g. 'yxz'.

    '''
    
    def rot_about_x(view_orientation, theta_x):
        rot_x = torch.tensor([[1, 0, 0], [0, np.cos(theta_x), -np.sin(theta_x)], [0, np.sin(theta_x), np.cos(theta_x)]])
        view_orientation = torch.matmul(view_orientation, rot_x.type(torch.FloatTensor))
        return view_orientation
    
    def rot_about_y(view_orientation, theta_y):
        rot_y = torch.tensor([[np.cos(theta_y), 0, np.sin(theta_y)], [0, 1, 0], [-np.sin(theta_y), 0, np.cos(theta_y)]])
        view_orientation = torch.matmul(view_orientation, rot_y.type(torch.FloatTensor))
        return view_orientation
    
    def rot_about_z(view_orientation, theta_z):
        rot_z = torch.tensor([[np.cos(theta_z), -np.sin(theta_z), 0], [np.sin(theta_z), np.cos(theta_z), 0], [0, 0, 1]])
        view_orientation = torch.matmul(view_orientation, rot_z.type(torch.FloatTensor))
        return view_orientation
    
    
    theta_x = np.deg2rad(theta[0])
    theta_y = np.deg2rad(theta[1])
    theta_z = np.deg2rad(theta[2])
    
    for rotation_axis in list(rotation_order.lower()):
        if rotation_axis == 'x':
            view_orientation = rot_about_x(view_orientation, theta_x)
        elif rotation_axis == 'y':
            view_orientation = rot_about_y(view_orientation, theta_y)
        elif rotation_axis == 'z':
            view_orientation = rot_about_z(view_orientation, theta_z)
            
    M_view  = torch.eye(4)
    M_view[:3,:3] = view_orientation
    M_view[:3,3] = view_point
    M_view = M_view.unsqueeze(0) # add channel dimension -required for compatibility with pytorch samplers
            
    return M_view

""" Run the program"""

# create a dummy 3D volume. Load one if you have one.
#extent = [[0, 179], [0, 179], [0, 179]] # [[xmin, xmax], [ymin, ymax], [ymin, ymax]], in mm
#size = [100, 100, 100]
extent = [[-150, 150], [-150, 150], [-150, 150]] # [[xmin, xmax], [ymin, ymax], [ymin, ymax]], in mm
size = [270, 196, 315]
#size = [100, 100, 100]

#dicom_file_dir = '/home/david/Documents/PhD/CT_images/01/'
datafile=r'D:\iFIND00485\registration_compounded_image.mhd'
#reference = load_dicom_image_as_sitk(dicom_file_dir, extent, size)
reference = load_sitk(datafile, extent, size)
#reference = create_random_image(datafile,extent, size)


fig = plt.figure()
camera = Camera(fig) # setup to save gif
Output=None #initliase empty array to save images
# Define a viewpoint and a direction from where scan the reference. By default,
# ultrasound waves propagate along +y direction,

for theta_y in range(0,20,1):
   for theta_z in range (-11,2,2):
       for angle in range(28, -22, -1):  # test16 is (-40,0,1) (25,-25,1)

           #view_point = torch.tensor([0, -150, 0])  # relates to view position, can be substituted with the angle sweep parameter
           view_point = torch.tensor([0, -150, 0])  # relates to view position, can be substituted with the angle sweep parameter
           # view_point = torch.tensor([160, 90, 160]) # relates to view position, can be substituted with the angle sweep parameter
           view_orientation = torch.eye(3)
           M_view = perform_ordered_transform(view_point, view_orientation, [angle, theta_y, theta_z], 'xyz')
            #M_view = torch.eye(4)
            #M_view[:3, :3] = view_orientation
            #M_view[:3, 3] = view_point
            #M_view = M_view.unsqueeze(0)  # add channel dimension -required for compatibility with pytorch samplers

            # Define the properties of the 2D image. A 2D image will be a 3D image with a z dim of 1.
            # Depth in polar coordinates is along the x axis
           size = [150, 150, 1]
           extent = [[0, 250], [-45*np.pi/180.0, +45*np.pi/180.0], [0, 1]] # mm and radians
           spacing = [ (extent[i][1]-extent[i][0])/(np.max((size[i]-1, 1))) for i in range(3)]

           info_2D = {'origin': [ extent[i][0] for i in range(3)],
                       'spacing': spacing,
                       'size': size }

           ref_torch, info_ref = sitkToTorch(reference)
           ref_torch.unsqueeze_(0).unsqueeze_(0)  # add channel and batch size dims to be compatible with pytorch framework
           sampling_grid = affine_grid_cartesian2polar(M_view.inverse(), info_2D, info_ref, align_corners=True)
           polar = F.grid_sample(ref_torch, sampling_grid, align_corners=True)
            # convert to cartesian

           polar_sitk = torchToSitk(polar[0, ...].squeeze(0), info_2D)
           cartesian_sitk = Polar_to_Cartesian(polar_sitk)
           cartesian_torch, info_cartesian = sitkToTorch(cartesian_sitk)

           plt.imshow(cartesian_torch.squeeze(), cmap='gray')
           camera.snap()
           if Output is None:
               Output=cartesian_torch.unsqueeze(2)
           else:
               Output = np.concatenate((Output, cartesian_torch.unsqueeze(2)), axis=2)
            #plt.subplot(1, 3, 1)
            #plt.imshow(ref_torch[0, 0, :, :, 150].squeeze(), cmap='gray')
            #plt.title('Central slice of 3D image')
            #plt.subplot(1, 3, 2)
            #plt.imshow(polar.squeeze(), cmap='gray')
            #plt.title('Polar image')
            #plt.subplot(1, 3, 3)
            #plt.imshow(cartesian_torch.squeeze(), cmap='gray')
            #plt.title('Cartesian image')

       animation = camera.animate()
        #animation.save(r'C:\Users\ng20\Desktop\Guidance_Work\Compounded_Volumes\iFIND00485\test19.gif')
       output_folder = r'D:\iFIND00485'
       parameters ='_angle{}_thetay{}_thetaz{}'.format(angle, theta_y, theta_z)

       animation.save('{}/test{}.gif'.format(output_folder, parameters))

        #with h5py.File(r'C:\Users\ng20\Desktop\Guidance_Work\Compounded_Volumes\iFIND00485\test19.h5', 'w') as hf:
       with h5py.File('{}/test{}.h5'.format(output_folder, parameters), 'w') as hf:
           hf.create_dataset("Output",  data=Output)

