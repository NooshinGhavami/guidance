%% New way of saving labels based on 'closer' or 'further'
% In this script we want to save the labels according to whether it is
% getting closer to or further away from a standard view. To do this, to
% begin with I will take the slice and the slice beforehand to see if the
% current slice is closer or further from each standard view per patient.


%% Initially for patient 00509
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\509\';
subfolders = {'Guidance_abdomen\iFIND00509\Guidance_abdomen', 'Guidance_head\iFIND00509\Guidance_head', 'Guidance_femur\iFIND00509\Guidance_femur'};
label_files = {'abdo_labels_es.csv', 'head_labels_es.csv', 'femur_labels_es.csv'};
label_names = {'Abdominal', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[6,216,371]; % Abdomen
Index_Start{1}=[1,148,363];
Index{2}=[274,611]; % Head
Index_Start{2}=[198,606];
Index{3}=[6,20,145,196,348,376]; % Femur
Index_Start{3}=[1,13,76,181,340,362];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=2; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=3; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
%image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Next for patient 00542
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\542\';
subfolders = {'Guidance_Abdomen\iFIND00542\Guidance_Abdomen', 'Guidance_Head\iFIND00542\Guidance_Head', 'Guidance_Femur\iFIND00542\Guidance_Femur'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'Abdominal', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[231,296,493,520,762,809,1136,1188,1242]; % Abdomen
Index_Start{1}=[1,232,297,494,521,763,810,1137,1189];
Index{2}=[297,316,341,383,568,615,832,870,900,950]; % Head
Index_Start{2}=[1,298,317,342,384,569,616,833,871,901];
Index{3}=[227,256,320,342,639,679,723,739,1022,1204,1235,1272,1329]; % Femur
Index_Start{3}=[1,228,257,321,343,640,680,724,740,1023,1205,1236,1273];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=5; % Abdomen
Middle_Cluster{2}=5; % Head
Middle_Cluster{3}=6; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Next for patient 00544
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\Nooshin_find2\iFIND00544\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'abdo_labels_es.csv', 'head_labels_es.csv', 'femur_labels_es.csv'};
label_names = {'Abdominal', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[594]; % Abdomen
Index_Start{1}=[1];
Index{2}=[146]; % Head
Index_Start{2}=[11];
Index{3}=[332]; % Femur
Index_Start{3}=[1];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Next for patient 00507
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\507\Guidance\iFIND00507\';
subfolders = {'Guidance', 'Guidance', 'Guidance'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'Guidance;Abdominal', 'Guidance;Brain (Tv.)', 'Guidance;Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[701,756,785,863,886]; % Abdomen
Index_Start{1}=[640,749,771,858,872];
Index{2}=[40]; % Head
Index_Start{2}=[1];
Index{3}=[1162,1189,1263]; % Femur
Index_Start{3}=[1161,1180,1151];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';

for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00510
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\510\';
subfolders = {'guidance_abdomen\iFIND00510\guidance_abdomen', 'guidance_head\iFIND00510\guidance_head', 'guidance_femur\iFIND00510\guidance_femur'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'guidance_abdomen;Abdominal', 'guidance_head;Brain (Tv.)', 'guidance_femur;Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[47,106]; % Abdomen
Index_Start{1}=[1,56];
Index{2}=[24]; % Head
Index_Start{2}=[13];
Index{3}=[5]; % Femur
Index_Start{3}=[1];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 80]';

for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00511
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\511\Guidance\iFIND00511\';
subfolders = {'Guidance', 'Guidance', 'Guidance'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'Guidance;Abdominal', 'Guidance;Brain (Tv.)', 'Guidance;Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[721,1126,1331,1439]; % Abdomen
Index_Start{1}=[680,1095,1311,1411];
Index{2}=[6]; % Head
Index_Start{2}=[1];
Index{3}=[1263,1354,1447]; % Femur
Index_Start{3}=[1261,1351,1446];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';

for j=3:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00512
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\512\SweepGuidance\iFIND00512\';
subfolders = {'SweepGuidance', 'SweepGuidance', 'SweepGuidance'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'SweepGuidance;Abdominal', 'SweepGuidance;Brain (Tv.)', 'SweepGuidance;Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[244,649,2665,3203,3375,3584,3869,4396,5287,5950]; % Abdomen
Index_Start{1}=[196,648,2609,3116,3365,3496,3699,4056,5121,5908];
Index{2}=[19,120,671,761,808,1093,1367,1611,1638,1821,2224,2428,2685,2721,2844,3234,3895,4423,4998,5691,5982]; % Head
Index_Start{2}=[8,111,663,741,791,1084,1346,1476,1622,1810,2214,2419,2671,2694,2832,3220,3882,4408,4924,5680,5960];
Index{3}=[447,5661]; % Femur
Index_Start{3}=[446,5660];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=4; % Abdomen
Middle_Cluster{2}=13; % Head
Middle_Cluster{3}=2; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';

for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00513
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\513\GuidanceSweep\iFIND00513\';
subfolders = {'GuidanceSweep', 'GuidanceSweep', 'GuidanceSweep'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'GuidanceSweep;Abdominal', 'GuidanceSweep;Brain (Tv.)', 'GuidanceSweep;Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[963,1922]; % Abdomen
Index_Start{1}=[882,1861];
Index{2}=[48,1007,2956]; % Head
Index_Start{2}=[12,979,2921];
Index{3}=[2568]; % Femur
Index_Start{3}=[2403];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=2; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';

for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00515
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\515\guidance\iFIND00515\';
subfolders = {'guidance', 'guidance', 'guidance'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'guidance;Abdominal', 'guidance;Brain (Tv.)', 'guidance;Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[892]; % Abdomen
Index_Start{1}=[846];
Index{2}=[5,130]; % Head
Index_Start{2}=[1,76];
Index{3}=[1160,1571]; % Femur
Index_Start{3}=[1156,1531];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=2; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end




%% Initially for patient 00479
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\479\iFIND00479\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[186]; % Abdomen
Index_Start{1}=[81];
Index{2}=[5]; % Head
Index_Start{2}=[1];
Index{3}=[230]; % Femur
Index_Start{3}=[208];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = 322;%length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00480
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\480\iFIND00480\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156]; % Abdomen
Index_Start{1}=[56];
Index{2}=[27]; % Head
Index_Start{2}=[1];
Index{3}=[236]; % Femur
Index_Start{3}=[214];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';

for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = 367;%length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00493
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\493\iFIND00493\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[359,456]; % Abdomen
Index_Start{1}=[297,401];
Index{2}=[42,287]; % Head
Index_Start{2}=[1,269];
Index{3}=[548,612]; % Femur
Index_Start{3}=[547,560];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = 628;%length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00494
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\494\iFIND00494\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[36]; % Abdomen
Index_Start{1}=[7];
Index{2}=[]; % Head
Index_Start{2}=[];
Index{3}=[129,181,210]; % Femur
Index_Start{3}=[66,136,188];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = 330;%length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00495
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\495\iFIND00495\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[497,755,903,1042,1103]; % Abdomen
Index_Start{1}=[346,521,900,982,1095];
Index{2}=[41,238]; % Head
Index_Start{2}=[34,187];
Index{3}=[820,878,971,1081,1151]; % Femur
Index_Start{3}=[816,859,916,1071,1151];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=2; % Abdomen
Middle_Cluster{2}=2; % Head
Middle_Cluster{3}=2; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = 1303;%length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00496
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\496\iFIND00496\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[41,735,971,1076,1811]; % Abdomen
Index_Start{1}=[1,696,794,987,1124,1854];
Index{2}=[251,308,409,658,1823]; % Head
Index_Start{2}=[76,280,367,613];
Index{3}=[66]; % Femur
Index_Start{3}=[60];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=3; % Abdomen
Middle_Cluster{2}=4; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = 1918;%length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00497
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\497\iFIND00497\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[295,366,522]; % Abdomen
Index_Start{1}=[261,307,377];
Index{2}=[48]; % Head
Index_Start{2}=[32];
Index{3}=[554,723]; % Femur
Index_Start{3}=[541,601];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = 820;%length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00498
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\498\iFIND00498\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[229,311]; % Abdomen
Index_Start{1}=[156,248];
Index{2}=[132]; % Head
Index_Start{2}=[87];
Index{3}=[426,602]; % Femur
Index_Start{3}=[357,486];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = 609;%length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00499
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\499\iFIND00499\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[223,331]; % Abdomen
Index_Start{1}=[72,262];
Index{2}=[5]; % Head
Index_Start{2}=[1];
Index{3}=[]; % Femur
Index_Start{3}=[];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)-1
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = 643;%length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00500
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\500\iFIND00500\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[706,891,1083,1339,1766]; % Abdomen
Index_Start{1}=[467,729,1021,1321,1571];
Index{2}=[26,1006,1793]; % Head
Index_Start{2}=[1,946,1775];
Index{3}=[1241,1548]; % Femur
Index_Start{3}=[1181,1401];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=3; % Abdomen
Middle_Cluster{2}=2; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = 1806;%length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00501
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\501\iFIND00501\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[366,651]; % Abdomen
Index_Start{1}=[251,506];
Index{2}=[186,431]; % Head
Index_Start{2}=[106,399];
Index{3}=[783]; % Femur
Index_Start{3}=[722];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = 976;%length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00502
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\502\iFIND00502\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[114,480,671,898]; % Abdomen
Index_Start{1}=[41,456,636,896];
Index{2}=[5,556,921]; % Head
Index_Start{2}=[1,505,901];
Index{3}=[198,441,730,892]; % Femur
Index_Start{3}=[151,244,711,753];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=2; % Abdomen
Middle_Cluster{2}=2; % Head
Middle_Cluster{3}=2; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = 928;%length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00503
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\503\iFIND00503\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[148,441]; % Abdomen
Index_Start{1}=[61,301];
Index{2}=[21,468]; % Head
Index_Start{2}=[1,454];
Index{3}=[241]; % Femur
Index_Start{3}=[221];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = 477;%length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00504
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\504\iFIND00504\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'abdo_labels.csv', 'head_labels.csv', 'femur_labels.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = 542;%length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00470
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\470\iFIND00470\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'470_labels_abdomen.csv', '470_labels_head.csv', '470_labels_femur.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=3%1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00471
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\471\iFIND00471\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'471_labels_abdomen.csv', '471_labels_head.csv', '471_labels_femur.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00472
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\472\iFIND00472\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'472_labels_abdomen.csv', '472_labels_head.csv', '472_labels_femur.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00473
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\473\iFIND00473\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'473_labels_abdomen.csv', '473_labels_head.csv', '473_labels_femur.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end



%% Initially for patient 00474
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\474\iFIND00474\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'474_labels_abdomen.csv', '474_labels_head.csv', '474_labels_femur.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00475
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\475\iFIND00475\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'475_labels_abdomen.csv', '475_labels_head.csv', '475_labels_femur.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00476
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\476\iFIND00476\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'476_labels_abdomen.csv', '476_labels_head.csv', '476_labels_femur.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00477
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\477\iFIND00477\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'477_labels_abdomen.csv', '477_labels_head.csv', '477_labels_femur.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=3%1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00478
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\478\iFIND00478\iFIND00478\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'478_labels_abdomen.csv', '478_labels_head.csv', '478_labels_femur.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00482
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\482\iFIND00482\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'482_labels_abdomen.csv', '482_labels_head.csv', '482_labels_femur.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=3%:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = 4237;%length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00483
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\483\iFIND00483\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'483_labels_abdomen.csv', '483_labels_head.csv', '483_labels_femur.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=2:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00484
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\484\iFIND00484\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'484_labels_abdomen.csv', '484_labels_head.csv', '484_labels_femur.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00485
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\485\iFIND00485\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'485_labels_abdomen.csv', '485_labels_head.csv', '485_labels_femur.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00486
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\486\iFIND00486\iFIND00486\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'486_labels_abdomen.csv', '486_labels_head.csv', '486_labels_femur.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00487
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\487\iFIND00487\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'487_labels_abdomen.csv', '487_labels_head.csv', '487_labels_femur.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00488
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\488\iFIND00488\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'488_labels_abdomen.csv', '488_labels_head.csv', '488_labels_femur.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
%image_centre = [0 80]';
image_centre = [0 0]';


for j=2:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        %currentimage = resampleImage(currentimage, [],'spacing_and_size_and_centre',[image_spacing ; image_size; image_centre]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00489
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\489\iFIND00489\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'489_labels_abdomen.csv', '489_labels_head.csv', '489_labels_femur.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00490
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\490\iFIND00490\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'490_labels_abdomen.csv', '490_labels_head.csv', '490_labels_femur.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end



%% Initially for patient 00430
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\430\iFIND00430\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'430_labels_abdomen_ES.csv', '430_labels_head_ES.csv', '430_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end
 
 
%% Initially for patient 00431
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\431\iFIND00431\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'431_labels_abdomen_ES.csv', '431_labels_head_ES.csv', '431_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end



%% Initially for patient 00432
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\432\iFIND00432\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'432_labels_abdomen_ES.csv', '432_labels_head_ES.csv', '432_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00433
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\433\iFIND00433\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'433_labels_abdomen_ES.csv', '433_labels_head_ES.csv', '433_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00434
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\434\iFIND00434\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'434_labels_abdomen_ES.csv', '434_labels_head_ES.csv', '434_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00435
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\435\iFIND00435\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'435_labels_abdomen_ES.csv', '435_labels_head_ES.csv', '435_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00436
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\436\iFIND00436\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'436_labels_abdomen_ES.csv', '436_labels_head_ES.csv', '436_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00437
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\437\iFIND00437\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'437_labels_abdomen_ES.csv', '437_labels_head_ES.csv', '437_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00438
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\438\iFIND00438\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'438_labels_abdomen_ES.csv', '438_labels_head_ES.csv', '438_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00439
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\439\iFIND00439\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'439_labels_abdomen_ES.csv', '439_labels_head_ES.csv', '439_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00440
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\440\iFIND00440\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'440_labels_abdomen_ES.csv', '440_labels_head_ES.csv', '440_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00441
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\441\iFIND00441\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'441_labels_abdomen_ES.csv', '441_labels_head_ES.csv', '441_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00442
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\442\iFIND00442\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'442_labels_abdomen_ES.csv', '442_labels_head_ES.csv', '442_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00444
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\444\iFIND00444\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'444_labels_abdomen_ES.csv', '444_labels_head_ES.csv', '444_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00445
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\445\iFIND00445\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'445_labels_abdomen_ES.csv', '445_labels_head_ES.csv', '445_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00446
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\446\iFIND00446\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'446_labels_abdomen_ES.csv', '446_labels_head_ES.csv', '446_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00447
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\447\iFIND00447\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'447_labels_abdomen_ES.csv', '447_labels_head_ES.csv', '447_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00448
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\448\iFIND00448\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'448_labels_abdomen_ES.csv', '448_labels_head_ES.csv', '448_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00449
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\449\iFIND00449\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'449_labels_abdomen_ES.csv', '449_labels_head_ES.csv', '449_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00450
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\450\iFIND00450\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'450_labels_abdomen_ES.csv', '450_labels_head_ES.csv', '450_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00451
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\451\iFIND00451\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'451_labels_abdomen_ES.csv', '451_labels_head_ES.csv', '451_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00454
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\454\iFIND00454\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'454_labels_abdomen_ES.csv', '454_labels_head_ES.csv', '454_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00455
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\455\iFIND00455\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'455_labels_abdomen_ES.csv', '455_labels_head_ES.csv', '455_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=2:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00456
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\456\iFIND00456\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'456_labels_abdomen_ES.csv', '456_labels_head_ES.csv', '456_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00457
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\457\iFIND00457\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'457_labels_abdomen_ES.csv', '457_labels_head_ES.csv', '457_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00458
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\458\iFIND00458\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'458_labels_abdomen_ES.csv', '458_labels_head_ES.csv', '458_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00459
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\459\iFIND00459\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'459_labels_abdomen_ES.csv', '459_labels_head_ES.csv', '459_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00461
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\461\iFIND00461\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'461_labels_abdomen_ES.csv', '461_labels_head_ES.csv', '461_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00462
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\462\iFIND00462\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'462_labels_abdomen_ES.csv', '462_labels_head_ES.csv', '462_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end


%% Initially for patient 00465
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\465\iFIND00465\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'465_labels_abdomen_ES.csv', '465_labels_head_ES.csv', '465_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00466
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\466\iFIND00466\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'466_labels_abdomen_ES.csv', '466_labels_head_ES.csv', '466_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00467
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\467\iFIND00467\';
subfolders = {'Sweep2d', 'Sweep2d', 'Sweep2d'};
label_files = {'467_labels_abdomen_ES.csv', '467_labels_head_ES.csv', '467_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00468
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\468\iFIND00468\';
subfolders = {'Sweep', 'Sweep', 'Sweep'};
label_files = {'468_labels_abdomen_ES.csv', '468_labels_head_ES.csv', '468_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end

%% Initially for patient 00469
clear
%
data_folder = 'C:\Users\ng20\Desktop\Guidance_Work\guidance\469\iFIND00469\';
subfolders = {'Abdomen', 'Head', 'Limbs'};
label_files = {'469_labels_abdomen_ES.csv', '469_labels_head_ES.csv', '469_labels_femur_ES.csv'};
label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};

% Find the indices of the new smoothed curve which correspond to 1
Index{1}=[156,390]; % Abdomen
Index_Start{1}=[60,388];
Index{2}=[5,481]; % Head
Index_Start{2}=[1,422];
Index{3}=[199,351]; % Femur
Index_Start{3}=[198,233];

% Store the index of the middle standard plane cluster to use
Middle_Cluster{1}=1; % Abdomen
Middle_Cluster{2}=1; % Head
Middle_Cluster{3}=1; % Femur

% options
create_animations = true;
image_size = [128 128]';%[255 255]';
image_spacing = [1 1]';%[0.5 0.5]';
image_centre = [0 0]';


for j=1:numel(label_names)
    
    imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
    nfiles = length(imagefiles);    % Number of files found
    dataset = [];
    for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
        [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
        currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
        images_patient{j}(i) = currentimage; % Save the image
        dataset(:,:,i) = images_patient{j}(i).data;
        if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
            if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                for k=1:numel(f)
                    info = rmfield(info, f{k});
                end
                
            else
                f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                for k=1:numel(f)
                    info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                end
                
            end
        end
        
        images_info_patient{j}(i) = info; % Store the information
    end
    label_str = label_names{j};
    label_str(regexp(label_str,'[ .()]'))=[];
    
    % compute proximity based on labels / create animation
    output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
    label_file = [data_folder '/' label_files{j} ];
    if create_animations
        %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
    else
        %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        [labels, direction] = compute_direction_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
    end
    
    % save data to h5 files
    h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
    if exist(h5_filename, 'file')==2
        delete(h5_filename);
    end
    h5create(h5_filename,'/Labels', size(labels));
    h5write(h5_filename,'/Labels', labels);
    
    h5create(h5_filename,'/Direction', size(direction));
    h5write(h5_filename,'/Direction', direction);
    
    h5create(h5_filename, '/Data',size(dataset));
    h5write(h5_filename, '/Data',dataset);
    disp(['Data written for view ' label_names{j}])
end