"""
Dataset loading

"""

import torch.utils.data as Data
import os
#import SimpleITK as sitk
import h5py
import numpy as np
import csv
import torchvision.transforms as torchtransforms
import torch


class guidancedataset_proximity(Data.Dataset):
    """
    A simple dataset that will take medical images from a folder and provide image
    and metadata
    Arguments
        transform (torch.Transform)
        datafile (string) filename of a text file with an hdf5 file per row.
        preload (bool) load all data to memory, or just load from file on demand
    """
    def __init__(self, datafile, transform=None, preload=False):

        self.datafile = datafile
        self.transform = transform
        self.preload = preload
        self.debug = True

        # Find the input files
        self.h5_filenames = [line.strip() for line in open(datafile)]
        # count thenumber of elements per file.
        self.nsamples = 0
        self.file_range = ()
        for i in range(len(self.h5_filenames)):
            hf = h5py.File(self.h5_filenames[i], 'r')
            data = hf['Data']
            self.nsamples += data.shape[0]
            self.file_range += (self.nsamples,)

        if self.debug == True:
            print('[guidancedataset] : datasets have:')
            for i in range(len(self.h5_filenames)):
                ns = self.file_range[i]
                if i >0:
                    ns-=self.file_range[i-1]
                print('{} has {} samples'.format(self.h5_filenames[i], ns))

        self.images = None
        self.labels = None
        if self.preload:
            for i in range(len(self.h5_filenames)):
                hf = h5py.File(self.h5_filenames[i], 'r')
                if self.images is None:
                    self.images = hf.get('Data')[()]
                    self.labels = hf.get('Proximity')[()]
                else:
                    data_ = hf.get('Data')
                    labels_ = hf.get('Proximity')
                    # previous_rows = self.images.shape[0]
                    # self.images.resize((previous_rows+data_.shape[0],self.images.shape[1], self.images.shape[2]))
                    # self.images[previous_rows:previous_rows+data_.shape[0],:,:] = data_
                    # self.labels.resize((1,previous_rows + labels_.shape[1]))
                    # self.labels[:,previous_rows:previous_rows + labels_.shape[1], :, :] = labels_
                    self.images = np.concatenate((self.images, data_[()]), axis=0)
                    self.labels = np.concatenate((self.labels, labels_[()]), axis=1)


    def index_to_file_index(self, idx):
        file_id = 0
        index_in_file_id = 0
        range_starts = (0,)+ self.file_range

        for ind, v in enumerate(self.file_range):
            if idx < v:
                file_id = ind
                index_in_file_id = idx-range_starts[ind]
                break

        return file_id, index_in_file_id

    def __len__(self):
        return self.nsamples

    def __getitem__(self, index):
        """

        Arguments
            index (int) index position to return the data

        Returns
            image from the input index, and labels form metadata
        """

        if self.preload:
            img = self.images[index, :, :]  # .astype(np.float32)
            l = self.labels[:, index]
        else:
            (file_id, index_in_file_id) = self.index_to_file_index(index)
            hf = h5py.File(self.h5_filenames[file_id], 'r')
            img = hf['Data'][index_in_file_id,...]
            l = hf['Proximity'][:,index_in_file_id]

        if self.transform is not None:
            img = self.transform(np.array(img, dtype=np.float32)) # needed to cast to enable torch transforms for augmentation

        return img, l


class guidancedataset_direction(Data.Dataset):
    """
    A simple dataset that will take medical images from a folder and provide image
    and metadata
    Arguments
        transform (torch.Transform)
        datafile (string) filename of a text file with an hdf5 file per row.
        preload (bool) load all data to memory, or just load from file on demand
    """
    def __init__(self, datafile, transform=None, preload=False, random_direction_flip = False, sequence_length=3, inter_frame_distance=1, learning_rate=0.01):

        self.datafile = datafile
        self.transform = transform
        self.preload = preload
        self.debug = True
        self.random_direction_flip = random_direction_flip
        self.sequence_length = sequence_length
        self.learning_rate = learning_rate
        if self.sequence_length <= 1:
            self.inter_frame_distance = 1
        else:
            self.inter_frame_distance = inter_frame_distance

        # Find the input files
        self.h5_filenames = [line.strip() for line in open(datafile)]
        # count thenumber of elements per file.
        self.nsamples = 0
        self.file_range = ()
        for i in range(len(self.h5_filenames)):
            hf = h5py.File(self.h5_filenames[i], 'r')
            data = hf['Data']

            # adjust this with sequence length and distance between frames
            samples_from_current_file = data.shape[0] - (self.inter_frame_distance * (self.sequence_length-1))
            self.nsamples += samples_from_current_file
            self.file_range += (self.nsamples,)

        if self.debug == True:
            print('[guidancedataset] : datasets have:')
            for i in range(len(self.h5_filenames)):
                ns = self.file_range[i]
                if i >0:
                    ns-=self.file_range[i-1]
                print('{} has {} samples'.format(self.h5_filenames[i], ns))

        self.images = None
        self.labels = None
        if self.preload:
            for i in range(len(self.h5_filenames)):
                hf = h5py.File(self.h5_filenames[i], 'r')
                if self.images is None:
                    self.images = hf.get('Data')[()]
                    self.labels = hf.get('Direction')[()]
                else:
                    data_ = hf.get('Data')
                    labels_ = hf.get('Direction')
                    # previous_rows = self.images.shape[0]
                    # self.images.resize((previous_rows+data_.shape[0],self.images.shape[1], self.images.shape[2]))
                    # self.images[previous_rows:previous_rows+data_.shape[0],:,:] = data_
                    # self.labels.resize((1,previous_rows + labels_.shape[1]))
                    # self.labels[:,previous_rows:previous_rows + labels_.shape[1], :, :] = labels_
                    self.images = np.concatenate((self.images, data_[()]), axis=0)
                    self.labels = np.concatenate((self.labels, labels_[()]), axis=0)


    def index_to_file_index(self, idx):
        # todo correct this with sequence_length and inter_frame_distance
        print('[ WARNING] guidancedataset.py : index_to_file_index | still not implemented when not pre-loading the data')
        file_id = 0
        index_in_file_id = 0
        range_starts = (0,)+ self.file_range

        for ind, v in enumerate(self.file_range):
            if idx < v:
                file_id = ind
                index_in_file_id = idx-range_starts[ind]
                break

        return file_id, index_in_file_id

    def __len__(self):
        return self.nsamples

    def get_name(self):
        return '_sl{}_ifd{}_rfa{}_lr{}'.format(self.sequence_length, self.inter_frame_distance, self.random_direction_flip, self.learning_rate)

    def __getitem__(self, index):
        """

        Arguments
            index (int) index position to return the data

        Returns
            image from the input index, and labels form metadata
        """

        if self.preload:
            # 'index' will be the index of the first frame in the sequence;
            # adjust the index of the current (last) frame in the sequce  to accomodate for the sequence buffer
            index_end = index+(self.sequence_length-1)*self.inter_frame_distance
            index_init = index
            #img = self.images[index, :, :]  # .astype(np.float32)
            img = self.images[index_init:index_end+1:self.inter_frame_distance, :, :]  # .astype(np.float32)

            # now adjust for the labels
            l = self.labels[index_end, :]
            l_flip = self.labels[index_init, :]
        else:
            # todo correct this with sequence_length and inter_frame_distance
            print('[ WARNING] guidancedataset.py : __get_item__ | still not implemented when not pre-loading the data')
            (file_id, index_in_file_id) = self.index_to_file_index(index)
            hf = h5py.File(self.h5_filenames[file_id], 'r')
            img = hf['Data'][index_in_file_id,...]
            #l = hf['Direction'][:,index_in_file_id]
            l = hf['Direction'][index_in_file_id, :]

        if self.random_direction_flip:
            do_flip = np.random.randint(0,2)
            if do_flip == 1:
                img = img[::-1, ...]
                l = 1-l_flip

        # Because images are numpy arrays, and the transformation to torch will swap dimensions, we need to move the
        # frame direction to the second position so that it ends up first
        img = img.transpose(1, 2, 0)

        if self.transform is not None:
            #img = self.transform(np.array(img, dtype=np.float32)) # needed to cast to enable torch transforms for augmentation
            img_new=None
            for frames in range((img.shape[2])):
                img_transformed = self.transform(np.array(img[:,:,frames], dtype=np.uint8)) # needed to cast to enable torch transforms for augmentation
                if img_new is None:
                    img_new = img_transformed
                else:
                    img_new = torch.cat((img_new, img_transformed), axis=0)
            img=img_new

        return img, l, index

class guidancedataset_direction_smallneighbourhood(Data.Dataset):
    """
    A simple dataset that will take medical images from a folder and provide image
    and metadata. It will only take a small number of slices around the standard view
    index to be near the vicinity of the standard view
    Arguments
        transform (torch.Transform)
        datafile (string) filename of a text file with an hdf5 file per row.
        preload (bool) load all data to memory, or just load from file on demand
    """
    def __init__(self, datafile, transform=None, preload=False, random_direction_flip = False, sequence_length=3, inter_frame_distance=1, learning_rate=0.01, neighbourhood=2):

        self.datafile = datafile
        self.transform = transform
        self.preload = preload
        self.debug = True
        self.random_direction_flip = random_direction_flip
        self.sequence_length = sequence_length
        self.learning_rate = learning_rate
        self.neighbourhood = neighbourhood
        if self.sequence_length <= 1:
            self.inter_frame_distance = 1
        else:
            self.inter_frame_distance = inter_frame_distance

        # Find the input files
        self.h5_filenames = [line.strip() for line in open(datafile)]
        # count thenumber of elements per file.
        self.nsamples = 0
        self.file_range = ()
        for i in range(len(self.h5_filenames)):
            hf = h5py.File(self.h5_filenames[i], 'r')
            data = hf['Data']

            # adjust this with sequence length and distance between frames
            samples_from_current_file = data.shape[0] - (self.inter_frame_distance * (self.sequence_length-1))
            self.nsamples += samples_from_current_file
            self.file_range += (self.nsamples,)

        if self.debug == True:
            print('[guidancedataset] : datasets have:')
            for i in range(len(self.h5_filenames)):
                ns = self.file_range[i]
                if i >0:
                    ns-=self.file_range[i-1]
                print('{} has {} samples'.format(self.h5_filenames[i], ns))

        self.images = None
        self.labels = None
        self.images_neighbourhood = None
        self.labels_neighbourhood = None
        if self.preload:
            for i in range(len(self.h5_filenames)):
                hf = h5py.File(self.h5_filenames[i], 'r')
                if self.images is None:
                    self.images = hf.get('Data')[()]
                    self.labels = hf.get('Direction')[()]
                    # Find where the standard view is (going from 1 to 0) and take only a small number of slices around that
                    for j in range(1,len(self.labels)):
                        if self.labels[j][0]==0 and self.labels[j-1][0]==1:
                            if self.images_neighbourhood is None:
                                self.labels_neighbourhood=self.labels[j-self.neighbourhood:j+self.neighbourhood+1] #take two slices around standard view only
                                self.images_neighbourhood=self.images[j-self.neighbourhood:j+self.neighbourhood+1,:,:]
                            else:
                                dataneighbourhood_=self.images[j-self.neighbourhood:j+self.neighbourhood+1,:,:]
                                labelsneighbourhood_ = self.labels[j-self.neighbourhood:j+self.neighbourhood+1]
                                self.images_neighbourhood = np.concatenate((self.images_neighbourhood, dataneighbourhood_[()]), axis=0)
                                self.labels_neighbourhood = np.concatenate((self.labels_neighbourhood, labelsneighbourhood_[()]), axis=0)
                        else:
                            continue


                else:
                    data_ = hf.get('Data')
                    labels_ = hf.get('Direction')
                    # Find where the standard view is (going from 1 to 0) and take only a small number of slices around that
                    for j in range(1, len(labels_)):
                        if labels_[j][0] == 0 and labels_[j - 1][0] == 1:
                            if self.images_neighbourhood is None:
                                self.labels_neighbourhood = labels_[j-self.neighbourhood:j+self.neighbourhood+1]  # take two slices around standard view only
                                self.images_neighbourhood = data_[j-self.neighbourhood:j+self.neighbourhood+1, :, :]
                            else:
                                dataneighbourhood_ = data_[j-self.neighbourhood:j+self.neighbourhood+1, :, :]
                                labelsneighbourhood_ = labels_[j-self.neighbourhood:j+self.neighbourhood+1]
                                self.images_neighbourhood = np.concatenate((self.images_neighbourhood, dataneighbourhood_[()]), axis=0)
                                self.labels_neighbourhood = np.concatenate((self.labels_neighbourhood, labelsneighbourhood_[()]), axis=0)
                        else:
                            continue

                    #self.images = np.concatenate((self.images, data_[()]), axis=0)
                    #self.labels = np.concatenate((self.labels, labels_[()]), axis=0)
                    self.nsamples = len(self.images_neighbourhood)


    def index_to_file_index(self, idx):
        # todo correct this with sequence_length and inter_frame_distance
        print('[ WARNING] guidancedataset.py : index_to_file_index | still not implemented when not pre-loading the data')
        file_id = 0
        index_in_file_id = 0
        range_starts = (0,)+ self.file_range

        for ind, v in enumerate(self.file_range):
            if idx < v:
                file_id = ind
                index_in_file_id = idx-range_starts[ind]
                break

        return file_id, index_in_file_id

    def __len__(self):
        return self.nsamples

    def get_name(self):
        return '_sl{}_ifd{}_rfa{}_lr{}_neighbourhood{}_'.format(self.sequence_length, self.inter_frame_distance, self.random_direction_flip, self.learning_rate, self.neighbourhood)

    def __getitem__(self, index):
        """

        Arguments
            index (int) index position to return the data

        Returns
            image from the input index, and labels form metadata
        """

        if self.preload:
            # 'index' will be the index of the first frame in the sequence;
            # adjust the index of the current (last) frame in the sequce  to accomodate for the sequence buffer
            if index > len(self.images_neighbourhood)-3:
                index=len(self.images_neighbourhood)-3
            index_end = index+(self.sequence_length-1)*self.inter_frame_distance
            index_init = index
            #if index_end==785:
            #    print('a')
            #img = self.images[index, :, :]  # .astype(np.float32)
            img = self.images_neighbourhood[index_init:index_end+1:self.inter_frame_distance, :, :]  # .astype(np.float32)

            # now adjust for the labels
            #if index_end >= len(self.labels_neighbourhood):
            #    index_end=len(self.labels_neighbourhood)-1
            l = self.labels_neighbourhood[index_end, :]
            l_flip = self.labels_neighbourhood[index_init, :]
        else:
            # todo correct this with sequence_length and inter_frame_distance
            print('[ WARNING] guidancedataset.py : __get_item__ | still not implemented when not pre-loading the data')
            (file_id, index_in_file_id) = self.index_to_file_index(index)
            hf = h5py.File(self.h5_filenames[file_id], 'r')
            img = hf['Data'][index_in_file_id,...]
            #l = hf['Direction'][:,index_in_file_id]
            l = hf['Direction'][index_in_file_id, :]

        if self.random_direction_flip:
            do_flip = np.random.randint(0,2)
            if do_flip == 1:
                img = img[::-1, ...]
                l = 1-l_flip

        # Because images are numpy arrays, and the transformation to torch will swap dimensions, we need to move the
        # frame direction to the second position so that it ends up first
        img = img.transpose(1, 2, 0)

        if self.transform is not None:
            #img = self.transform(np.array(img, dtype=np.float32)) # needed to cast to enable torch transforms for augmentation
            img_new=None
            for frames in range((img.shape[2])):
                img_transformed = self.transform(np.array(img[:,:,frames], dtype=np.uint8)) # needed to cast to enable torch transforms for augmentation
                if img_new is None:
                    img_new = img_transformed
                else:
                    img_new = torch.cat((img_new, img_transformed), axis=0)
            img=img_new

        return img, l