%% Make videos from some of the iFIND2 images and save as .mp4

Patient_509=h5read('C:\Users\ng20\Desktop\Guidance_Work\guidance\509\Guidance_abdomen\iFIND00509\Guidance_abdomen\abdo_labels_es_Abdominal.h5','/Data');
Patient_496=h5read('C:\Users\ng20\Desktop\Guidance_Work\guidance\496\iFIND00496\Sweep\abdo_labels_Abdomen.h5','/Data');
Patient_500=h5read('C:\Users\ng20\Desktop\Guidance_Work\guidance\500\iFIND00500\Sweep\abdo_labels_Abdomen.h5','/Data');

video = VideoWriter('500.mp4'); %create the video object
open(video); %open the file for writing
for ii=1:size(Patient_500,3) %where N is the number of images
  I = mat2gray(Patient_500(:,:,ii)); %read the next image
  writeVideo(video,I); %write the image to file
end
close(video); %close the file