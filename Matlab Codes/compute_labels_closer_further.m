function [Closeness] = compute_labels_closer_further(image_list, image_info_list, label_file, labelName, label_indices, start_indices, middle_cluster, output_gif_filename,flag)
%compute_proximity_values COmputes the proximity values and returns the
%ground truth labels too.
% If the gif file is not [], an animation s created/

tonumber= @(x) strcmp(x,labelName);

if flag==2 % for patient 2 Abdomen
    Linear_Index_Abdomen_Patient2=zeros(1,size(image_list,2));
    Index_Abdomen_New_Patient2=[218,231,296,479,493,505,520,762,773,789,809,1136,1152,1172,1188,1242];
    Linear_Index_Abdomen_Patient2(Index_Abdomen_New_Patient2)=1;
    labels = Linear_Index_Abdomen_Patient2;
elseif flag==4 % for patient 2 head
    Linear_Index_Head_Patient2=zeros(1,size(image_list,2));
    Index_Head_New_Patient2=[297,316,341,383,568,597,615,832,870,885,900,950];
    Linear_Index_Head_Patient2(Index_Head_New_Patient2)=1;
    labels = Linear_Index_Head_Patient2;
elseif flag==6 % for patient 2 femur
    Linear_Index_Femur_Patient2=zeros(1,size(image_list,2));
    Index_Femur_New_Patient2=[227,256,271,285,298,306,320,342,639,652,666,679,705,723,739,1022,1204,1220,1235,1272,1329];
    Linear_Index_Femur_Patient2(Index_Femur_New_Patient2)=1;
    labels = Linear_Index_Femur_Patient2;
elseif flag==3 % for patient 3 Abdomen
    Linear_Index_Abdomen_Patient3=zeros(1,size(image_list,2));
    Index_Abdomen_New_Patient3=[594];
    Linear_Index_Abdomen_Patient3(Index_Abdomen_New_Patient3)=1;
    labels = Linear_Index_Abdomen_Patient3;
% elseif flag==6 % for patient 3 Head
%     Linear_Index_Head_Patient3=zeros(1,size(image_list,2));
%     Index_Head_New_Patient3=[146];
%     Linear_Index_Head_Patient3(Index_Head_New_Patient3)=1;
%     labels = Linear_Index_Head_Patient3;
elseif flag==9 % for patient 3 Femur
    Linear_Index_Femur_Patient3=zeros(1,size(image_list,2));
    Index_Femur_New_Patient3=[332];
    Linear_Index_Femur_Patient3(Index_Femur_New_Patient3)=1;
    labels = Linear_Index_Femur_Patient3;
else
   abdo_labels = readtable(label_file, 'Delimiter' , ',','ReadVariableNames',false);
   labels = double(cellfun(tonumber,table2array(abdo_labels(:,2))));
   labels=labels(1:end);
end


% define the closer and further from the labels
% Find the slice corresponding to the indices of the smoothed curve
% corresponding to the correct standard planes and find the difference (in
% terms of time) from each image to the reference slices.
timestamps = zeros(size(image_info_list,2),1);
for i=1:size(image_info_list,2)
   timestamps(i) = str2num(image_info_list(i).DNLTimestamp)/1000;
end
timestamp_sd_index = timestamps(label_indices);
distance = ones(size(timestamps))*Inf;
for i=1:numel(label_indices)
    d = abs(timestamps-timestamp_sd_index(i));
    distance = [distance d];
end
X=1:size(image_info_list,2);
B=label_indices-X';
B(B>0)=1; B(B<0)=0;
for i=1:size(B,2)
distance(:,i+1)=distance(:,i+1).*B(:,i);
end
distance(distance>0)=1;
%Closeness=distance(:,2:end);
Closeness={};%zeros(size(distance,1),3);
dist_oneSP=distance(:,middle_cluster+1);
Closeness{1}=distance(:,middle_cluster+1); % This is whether it's getting closer or further
Array_1=[(1:size(distance,1))', (0:size(distance,1)-1)']; % This defines when taking two consecutive frames
Array_1(:,2)=Array_1(:,2)-4; % Take every five slices instead of consecutive ones since the consecutive ones are too close to another
Array_1(Array_1<1)=1;
Array_2=[(1:size(distance,1))' (0:size(distance,1)-1)' (-1:size(distance,1)-2)']; % This defines when taking three consecutive frames
Array_2(:,2)=Array_2(:,2)-4; % Take every five slices instead of consecutive ones since the consecutive ones are too close to another
Array_2(:,3)=Array_2(:,3)-8; % Take every five slices instead of consecutive ones since the consecutive ones are too close to another
Array_2(Array_2<1)=1;

% For array 1
for idx = 1:length(Array_1)
Out_Array1{idx} = Array_1(idx,:);
end
% For array 2
for idx = 1:length(Array_2)
Out_Array2{idx} = Array_2(idx,:);
end
Closeness{2}=Out_Array1';
Closeness{3}=Out_Array2';

% define time axis
t = zeros(size(labels));
for i = 1:numel(labels)
    t(i) = str2num(image_info_list(i).DNLLayerTimeTag)/1000000; % dnl layer comes in microseconds
end
t = t-t(1); % in seconds
framerate = median(1./(t(2:end)-t(1:end-1)));


if numel(output_gif_filename) > 0
    
    sz = size(image_list(1).data);
    
    subplot(2,1,1)
    h = imshow(image_list(1).data',[] ); set(h,'erasemode','xor');
    h_highlight = [];
    title(['Not ' labelName ' view'])
    subplot(2,1,2)
    plot(t, dist_oneSP(1:end),'LineWidth',2)
    hold on;
    plot(t, labels,'x','MarkerSize',5)
    hold off;
    xlabel('time (s)')
    for i = 1:numel(labels)
        subplot(2,1,1)
        set(h,'cdata',image_list(i).data')
        hold on
        if labels(i)==1
            h_highlight  = plot([1 sz(1) sz(1) 1 1],[1 1 sz(2) sz(2) 1], 'r-','LineWidth',10);
            title(['*** ' labelName ' view ***'])
        else
            title(['Not ' labelName ' view'])
        end
        rectangle('Position',[20 20 25 200],'EdgeColor','white')
        h_barfil = rectangle('Position',[20 20+200*(1-dist_oneSP(i)) 25 200*dist_oneSP(i)],'EdgeColor','white','FaceColor','white');
        hold off
        
        subplot(2,1,2)
        hold on;
        h_line = plot([t(i) t(i)],[-0.1 1.1],'k--','LineWidth',2);
        hold off;
        xlim([t(1) t(end)])
        ylim([-0.1 1.1])
        legend({'proximity to view', 'is Std View? (GT)', 'current frame'},'Location','bestoutside')
        
        % Capture the plot as an image
        frame = getframe(gcf);
        im = frame2im(frame);
        [imind,cm] = rgb2ind(im,256);
        % Write to the GIF File
        if i == 1
            imwrite(imind,cm,output_gif_filename,'gif', 'Loopcount',inf,'DelayTime',1/framerate);
        else
            imwrite(imind,cm,output_gif_filename,'gif','WriteMode','append','DelayTime',1/framerate);
        end
        
        delete(h_highlight )
        delete(h_barfil)
        delete(h_line)
    end
    close(gcf)
end
end
