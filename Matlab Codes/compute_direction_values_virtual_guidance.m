function [labels, direction] = compute_direction_values_virtual_guidance(image_list, image_info_list, label_file, labelName, output_gif_filename)
%compute_proximity_values COmputes the proximity values and returns the
%ground truth labels too.
% If the gif file is not [], an animation s created/



tonumber = @(x) contains(x,labelName);
%read_labels= readtable(label_file, 'Delimiter' , ',','ReadVariableNames',false);

%labels = double(cellfun(tonumber,table2array(read_labels(:,end))));
%labels=labels(2:end);
labels=zeros(size(image_list,2),1); % make labels all zero the size of the number of images
labels(end)=1; % the last value will be 1 where the standard plane of the absomen/head/femur is

% For each interval, pick the central point
ranges = [];
in_a_range = false;
n_ranges = 0;
for i = 1:numel(labels)
    
    if labels(i) ==1
        if ~in_a_range
            n_ranges = n_ranges+1;
            ranges(n_ranges,1) = i;
            in_a_range = true;
        end
    else
        if in_a_range
            ranges(n_ranges, 2) = i-1;
            in_a_range = false;
        end
    end
    
    
end

% centre of intervals
interval_centres = round(mean(ranges,2));

% for test

indices = 1:numel(labels);
distance_to_standard_plane = interval_centres - indices ;
[~, ind] = min(abs(distance_to_standard_plane'),[],2);
direction  =zeros(1, numel(labels));
for j=1:size(interval_centres,1)
    direction(ind==j) = distance_to_standard_plane(j,ind==j);
end
direction = sign(direction);
direction(direction<0) = 0;

t = 1:numel(labels);
framerate = t(end)/10; % make it last 10 seconds

if numel(output_gif_filename) > 0
    
    sz = size(image_list(1).data);
    
    subplot(2,1,1)
    h = imshow(image_list(1).data',[] ); set(h,'erasemode','xor');
    h_highlight = [];
    title(['Not ' labelName ' view'])
    subplot(2,1,2)
    plot(t, direction,'LineWidth',2)
    hold on;
    plot(t, labels,'x','MarkerSize',5)
    hold off;
    xlabel('time (s)')
    for i = 1:numel(labels)
        subplot(2,1,1)
        set(h,'cdata',image_list(i).data')
        hold on
        if labels(i)==1
            h_highlight  = plot([1 sz(1) sz(1) 1 1],[1 1 sz(2) sz(2) 1], 'r-','LineWidth',10);
            title(['*** ' labelName ' view ***'])
        else
            title(['Not ' labelName ' view'])
        end
        %rectangle('Position',[20 20 25 200],'EdgeColor','white')
        %h_barfil = rectangle('Position',[20 20+200*max(1-direction(i),eps) 25 200*max(direction(i),eps)],'EdgeColor','white','FaceColor','white');
        hold off
        
        subplot(2,1,2)
        hold on;
        h_line = plot([t(i) t(i)],[-0.1 1.1],'k--','LineWidth',2);
        hold off;
        xlim([t(1) t(end)])
        ylim([-0.1 1.1])
        legend({'direction to view', 'is Std View? (GT)', 'current frame'},'Location','bestoutside')
        
        % Capture the plot as an image
        frame = getframe(gcf);
        im = frame2im(frame);
        [imind,cm] = rgb2ind(im,256);
        % Write to the GIF File
        if i == 1
            imwrite(imind,cm,output_gif_filename,'gif', 'Loopcount',inf,'DelayTime',1/framerate);
        else
            imwrite(imind,cm,output_gif_filename,'gif','WriteMode','append','DelayTime',1/framerate);
        end
        
        delete(h_highlight )
        %delete(h_barfil)
        delete(h_line)
    end
    close(gcf)
end
end
