x = 0:0.01:1;
figure(1)
filename = '476.gif';
A=h5read('C:\Users\ng20\Desktop\Guidance_Work\guidance\476\iFIND00476\Abdomen\476_labels_abdomen_Abdomen.h5','/Data');
B=h5read('C:\Users\ng20\Desktop\Guidance_Work\guidance\476\iFIND00476\Abdomen\476_labels_abdomen_Abdomen.h5','/Direction');
for n = 1:9
      y = x.^n;
      %plot(x,y)
      imshow(A(:,:,n+60),[])
      drawnow
      frame = getframe(1);
      im = frame2im(frame);
      [imind,cm] = rgb2ind(im,256);
      if n == 1;
          imwrite(imind,cm,filename,'gif', 'Loopcount',inf);
      else
          imwrite(imind,cm,filename,'gif','WriteMode','append');
      end
end

