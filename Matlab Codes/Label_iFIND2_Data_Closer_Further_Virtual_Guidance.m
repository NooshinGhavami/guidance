%% Virtual Guidance Data
% In this script I will load the saved .h5 files from the vitual guidance
% python code and assign labels to them and save them for using into the
% guidance network.

%% Virtual Trajectories iFIND00061
clear
%
data_folder = 'D:\iFIND00061\';
for k=1:101
    subfolders = {['Abdomen\virtual_trajectory_' num2str(k-1)], ['Head\virtual_trajectory_' num2str(k-1)], ['Limbs\virtual_trajectory_' num2str(k-1)]};
    label_files = {'abdo_labels.csv', 'head_labels_es.csv', 'femur_labels_es.csv'};
    label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};
    
    
    % Store the index of the middle standard plane cluster to use
    Middle_Cluster{1}=2; % Abdomen
    Middle_Cluster{2}=1; % Head
    Middle_Cluster{3}=3; % Femur
    
    % options
    create_animations = true;
    image_size = [64 64]';%[255 255]';
    image_spacing = [2 2]';%[0.5 0.5]';
    %image_centre = [0 0]';
    
    
    for j=1%:numel(label_names)
        
        imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
        nfiles = length(imagefiles);    % Number of files found
        dataset = [];
        for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
            [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
            currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
            images_patient{j}(i) = currentimage; % Save the image
            dataset(:,:,i) = images_patient{j}(i).data;
            if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
                if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                    f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                    for k=1:numel(f)
                        info = rmfield(info, f{k});
                    end
                    
                else
                    f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                    for k=1:numel(f)
                        info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                    end
                    
                end
            end
            
            images_info_patient{j}(i) = info; % Store the information
        end
        label_str = label_names{j};
        label_str(regexp(label_str,'[ .()]'))=[];
        
        % compute proximity based on labels / create animation
        output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
        label_file = [data_folder '/' subfolders{j} '/' label_files{j} ];
        if create_animations
            %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
        else
            %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        end
        
        % save data to h5 files
        h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
        if exist(h5_filename, 'file')==2
            delete(h5_filename);
        end
        h5create(h5_filename,'/Labels', size(labels));
        h5write(h5_filename,'/Labels', labels);
        
        h5create(h5_filename,'/Direction', size(direction));
        h5write(h5_filename,'/Direction', direction);
        
        h5create(h5_filename, '/Data',size(dataset));
        h5write(h5_filename, '/Data',dataset);
        disp(['Data written for view ' label_names{j}])
    end
end


% Virtual Trajectories iFIND00102
clear
%
data_folder = 'D:\iFIND00102\';
for k=1:101
    subfolders = {['Abdomen\virtual_trajectory_' num2str(k-1)], ['Head\virtual_trajectory_' num2str(k-1)], ['Limbs\virtual_trajectory_' num2str(k-1)]};
    label_files = {'abdo_labels.csv', 'head_labels_es.csv', 'femur_labels_es.csv'};
    label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};
    
    
    % Store the index of the middle standard plane cluster to use
    Middle_Cluster{1}=2; % Abdomen
    Middle_Cluster{2}=1; % Head
    Middle_Cluster{3}=3; % Femur
    
    % options
    create_animations = true;
    image_size = [64 64]';%[255 255]';
    image_spacing = [2 2]';%[0.5 0.5]';
    %image_centre = [0 0]';
    
    
    for j=1%:numel(label_names)
        
        imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
        nfiles = length(imagefiles);    % Number of files found
        dataset = [];
        for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
            [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
            currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
            images_patient{j}(i) = currentimage; % Save the image
            dataset(:,:,i) = images_patient{j}(i).data;
            if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
                if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                    f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                    for k=1:numel(f)
                        info = rmfield(info, f{k});
                    end
                    
                else
                    f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                    for k=1:numel(f)
                        info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                    end
                    
                end
            end
            
            images_info_patient{j}(i) = info; % Store the information
        end
        label_str = label_names{j};
        label_str(regexp(label_str,'[ .()]'))=[];
        
        % compute proximity based on labels / create animation
        output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
        label_file = [data_folder '/' subfolders{j} '/' label_files{j} ];
        if create_animations
            %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
        else
            %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        end
        
        % save data to h5 files
        h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
        if exist(h5_filename, 'file')==2
            delete(h5_filename);
        end
        h5create(h5_filename,'/Labels', size(labels));
        h5write(h5_filename,'/Labels', labels);
        
        h5create(h5_filename,'/Direction', size(direction));
        h5write(h5_filename,'/Direction', direction);
        
        h5create(h5_filename, '/Data',size(dataset));
        h5write(h5_filename, '/Data',dataset);
        disp(['Data written for view ' label_names{j}])
    end
end


% Virtual Trajectories iFIND00103
clear
%
data_folder = 'D:\iFIND00103\';
for k=1:101
    subfolders = {['Abdomen\virtual_trajectory_' num2str(k-1)], ['Head\virtual_trajectory_' num2str(k-1)], ['Limbs\virtual_trajectory_' num2str(k-1)]};
    label_files = {'abdo_labels.csv', 'head_labels_es.csv', 'femur_labels_es.csv'};
    label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};
    
    
    % Store the index of the middle standard plane cluster to use
    Middle_Cluster{1}=2; % Abdomen
    Middle_Cluster{2}=1; % Head
    Middle_Cluster{3}=3; % Femur
    
    % options
    create_animations = true;
    image_size = [64 64]';%[255 255]';
    image_spacing = [2 2]';%[0.5 0.5]';
    %image_centre = [0 0]';
    
    
    for j=1%:numel(label_names)
        
        imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
        nfiles = length(imagefiles);    % Number of files found
        dataset = [];
        for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
            [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
            currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
            images_patient{j}(i) = currentimage; % Save the image
            dataset(:,:,i) = images_patient{j}(i).data;
            if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
                if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                    f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                    for k=1:numel(f)
                        info = rmfield(info, f{k});
                    end
                    
                else
                    f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                    for k=1:numel(f)
                        info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                    end
                    
                end
            end
            
            images_info_patient{j}(i) = info; % Store the information
        end
        label_str = label_names{j};
        label_str(regexp(label_str,'[ .()]'))=[];
        
        % compute proximity based on labels / create animation
        output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
        label_file = [data_folder '/' subfolders{j} '/' label_files{j} ];
        if create_animations
            %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
        else
            %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        end
        
        % save data to h5 files
        h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
        if exist(h5_filename, 'file')==2
            delete(h5_filename);
        end
        h5create(h5_filename,'/Labels', size(labels));
        h5write(h5_filename,'/Labels', labels);
        
        h5create(h5_filename,'/Direction', size(direction));
        h5write(h5_filename,'/Direction', direction);
        
        h5create(h5_filename, '/Data',size(dataset));
        h5write(h5_filename, '/Data',dataset);
        disp(['Data written for view ' label_names{j}])
    end
end


% Virtual Trajectories iFIND00105
clear
%
data_folder = 'D:\iFIND00105\';
for k=1:101
    subfolders = {['Abdomen\virtual_trajectory_' num2str(k-1)], ['Head\virtual_trajectory_' num2str(k-1)], ['Limbs\virtual_trajectory_' num2str(k-1)]};
    label_files = {'abdo_labels.csv', 'head_labels_es.csv', 'femur_labels_es.csv'};
    label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};
    
    
    % Store the index of the middle standard plane cluster to use
    Middle_Cluster{1}=2; % Abdomen
    Middle_Cluster{2}=1; % Head
    Middle_Cluster{3}=3; % Femur
    
    % options
    create_animations = true;
    image_size = [64 64]';%[255 255]';
    image_spacing = [2 2]';%[0.5 0.5]';
    %image_centre = [0 0]';
    
    
    for j=1%:numel(label_names)
        
        imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
        nfiles = length(imagefiles);    % Number of files found
        dataset = [];
        for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
            [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
            currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
            images_patient{j}(i) = currentimage; % Save the image
            dataset(:,:,i) = images_patient{j}(i).data;
            if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
                if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                    f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                    for k=1:numel(f)
                        info = rmfield(info, f{k});
                    end
                    
                else
                    f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                    for k=1:numel(f)
                        info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                    end
                    
                end
            end
            
            images_info_patient{j}(i) = info; % Store the information
        end
        label_str = label_names{j};
        label_str(regexp(label_str,'[ .()]'))=[];
        
        % compute proximity based on labels / create animation
        output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
        label_file = [data_folder '/' subfolders{j} '/' label_files{j} ];
        if create_animations
            %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
        else
            %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        end
        
        % save data to h5 files
        h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
        if exist(h5_filename, 'file')==2
            delete(h5_filename);
        end
        h5create(h5_filename,'/Labels', size(labels));
        h5write(h5_filename,'/Labels', labels);
        
        h5create(h5_filename,'/Direction', size(direction));
        h5write(h5_filename,'/Direction', direction);
        
        h5create(h5_filename, '/Data',size(dataset));
        h5write(h5_filename, '/Data',dataset);
        disp(['Data written for view ' label_names{j}])
    end
end

% Virtual Trajectories iFIND00106
clear
%
data_folder = 'D:\iFIND00106\';
for k=1:101
    subfolders = {['Abdomen\virtual_trajectory_' num2str(k-1)], ['Limbs\virtual_trajectory_' num2str(k-1)]};
    label_files = {'abdo_labels.csv',  'femur_labels_es.csv'};
    label_names = {'Abdomen',  'Femur'};
    
    
    % Store the index of the middle standard plane cluster to use
    Middle_Cluster{1}=2; % Abdomen
    Middle_Cluster{2}=1; % Head
    Middle_Cluster{3}=3; % Femur
    
    % options
    create_animations = true;
    image_size = [64 64]';%[255 255]';
    image_spacing = [2 2]';%[0.5 0.5]';
    %image_centre = [0 0]';
    
    
    for j=1%:numel(label_names)
        
        imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
        nfiles = length(imagefiles);    % Number of files found
        dataset = [];
        for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
            [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
            currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
            images_patient{j}(i) = currentimage; % Save the image
            dataset(:,:,i) = images_patient{j}(i).data;
            if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
                if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                    f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                    for k=1:numel(f)
                        info = rmfield(info, f{k});
                    end
                    
                else
                    f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                    for k=1:numel(f)
                        info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                    end
                    
                end
            end
            
            images_info_patient{j}(i) = info; % Store the information
        end
        label_str = label_names{j};
        label_str(regexp(label_str,'[ .()]'))=[];
        
        % compute proximity based on labels / create animation
        output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
        label_file = [data_folder '/' subfolders{j} '/' label_files{j} ];
        if create_animations
            %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
        else
            %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        end
        
        % save data to h5 files
        h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
        if exist(h5_filename, 'file')==2
            delete(h5_filename);
        end
        h5create(h5_filename,'/Labels', size(labels));
        h5write(h5_filename,'/Labels', labels);
        
        h5create(h5_filename,'/Direction', size(direction));
        h5write(h5_filename,'/Direction', direction);
        
        h5create(h5_filename, '/Data',size(dataset));
        h5write(h5_filename, '/Data',dataset);
        disp(['Data written for view ' label_names{j}])
    end
end


% Virtual Trajectories iFIND00109
clear
%
data_folder = 'D:\iFIND00109\';
for k=1:101
    subfolders = {['Abdomen\virtual_trajectory_' num2str(k-1)], ['Head\virtual_trajectory_' num2str(k-1)], ['Limbs\virtual_trajectory_' num2str(k-1)]};
    label_files = {'abdo_labels.csv', 'head_labels_es.csv', 'femur_labels_es.csv'};
    label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};
    
    
    % Store the index of the middle standard plane cluster to use
    Middle_Cluster{1}=2; % Abdomen
    Middle_Cluster{2}=1; % Head
    Middle_Cluster{3}=3; % Femur
    
    % options
    create_animations = true;
    image_size = [64 64]';%[255 255]';
    image_spacing = [2 2]';%[0.5 0.5]';
    %image_centre = [0 0]';
    
    
    for j=1%:numel(label_names)
        
        imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
        nfiles = length(imagefiles);    % Number of files found
        dataset = [];
        for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
            [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
            currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
            images_patient{j}(i) = currentimage; % Save the image
            dataset(:,:,i) = images_patient{j}(i).data;
            if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
                if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                    f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                    for k=1:numel(f)
                        info = rmfield(info, f{k});
                    end
                    
                else
                    f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                    for k=1:numel(f)
                        info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                    end
                    
                end
            end
            
            images_info_patient{j}(i) = info; % Store the information
        end
        label_str = label_names{j};
        label_str(regexp(label_str,'[ .()]'))=[];
        
        % compute proximity based on labels / create animation
        output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
        label_file = [data_folder '/' subfolders{j} '/' label_files{j} ];
        if create_animations
            %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
        else
            %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        end
        
        % save data to h5 files
        h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
        if exist(h5_filename, 'file')==2
            delete(h5_filename);
        end
        h5create(h5_filename,'/Labels', size(labels));
        h5write(h5_filename,'/Labels', labels);
        
        h5create(h5_filename,'/Direction', size(direction));
        h5write(h5_filename,'/Direction', direction);
        
        h5create(h5_filename, '/Data',size(dataset));
        h5write(h5_filename, '/Data',dataset);
        disp(['Data written for view ' label_names{j}])
    end
end


% Virtual Trajectories iFIND00110
clear
%
data_folder = 'D:\iFIND00110\';
for k=1:101
    subfolders = {['Abdomen\virtual_trajectory_' num2str(k-1)], ['Head\virtual_trajectory_' num2str(k-1)], ['Limbs\virtual_trajectory_' num2str(k-1)]};
    label_files = {'abdo_labels.csv', 'head_labels_es.csv', 'femur_labels_es.csv'};
    label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};
    
    
    % Store the index of the middle standard plane cluster to use
    Middle_Cluster{1}=2; % Abdomen
    Middle_Cluster{2}=1; % Head
    Middle_Cluster{3}=3; % Femur
    
    % options
    create_animations = true;
    image_size = [64 64]';%[255 255]';
    image_spacing = [2 2]';%[0.5 0.5]';
    %image_centre = [0 0]';
    
    
    for j=1%:numel(label_names)
        
        imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
        nfiles = length(imagefiles);    % Number of files found
        dataset = [];
        for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
            [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
            currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
            images_patient{j}(i) = currentimage; % Save the image
            dataset(:,:,i) = images_patient{j}(i).data;
            if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
                if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                    f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                    for k=1:numel(f)
                        info = rmfield(info, f{k});
                    end
                    
                else
                    f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                    for k=1:numel(f)
                        info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                    end
                    
                end
            end
            
            images_info_patient{j}(i) = info; % Store the information
        end
        label_str = label_names{j};
        label_str(regexp(label_str,'[ .()]'))=[];
        
        % compute proximity based on labels / create animation
        output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
        label_file = [data_folder '/' subfolders{j} '/' label_files{j} ];
        if create_animations
            %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
        else
            %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        end
        
        % save data to h5 files
        h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
        if exist(h5_filename, 'file')==2
            delete(h5_filename);
        end
        h5create(h5_filename,'/Labels', size(labels));
        h5write(h5_filename,'/Labels', labels);
        
        h5create(h5_filename,'/Direction', size(direction));
        h5write(h5_filename,'/Direction', direction);
        
        h5create(h5_filename, '/Data',size(dataset));
        h5write(h5_filename, '/Data',dataset);
        disp(['Data written for view ' label_names{j}])
    end
end


%% Virtual Trajectories iFIND00111
clear
%
data_folder = 'D:\iFIND00111\';
for k=1:101
    subfolders = {['Abdomen\virtual_trajectory_' num2str(k-1)], ['Head\virtual_trajectory_' num2str(k-1)], ['Limbs\virtual_trajectory_' num2str(k-1)]};
    label_files = {'abdo_labels.csv', 'head_labels_es.csv', 'femur_labels_es.csv'};
    label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};
    
    
    % Store the index of the middle standard plane cluster to use
    Middle_Cluster{1}=2; % Abdomen
    Middle_Cluster{2}=1; % Head
    Middle_Cluster{3}=3; % Femur
    
    % options
    create_animations = true;
    image_size = [64 64]';%[255 255]';
    image_spacing = [2 2]';%[0.5 0.5]';
    %image_centre = [0 0]';
    
    
    for j=1%:numel(label_names)
        
        imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
        nfiles = length(imagefiles);    % Number of files found
        dataset = [];
        for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
            [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
            currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
            images_patient{j}(i) = currentimage; % Save the image
            dataset(:,:,i) = images_patient{j}(i).data;
            if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
                if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                    f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                    for k=1:numel(f)
                        info = rmfield(info, f{k});
                    end
                    
                else
                    f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                    for k=1:numel(f)
                        info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                    end
                    
                end
            end
            
            images_info_patient{j}(i) = info; % Store the information
        end
        label_str = label_names{j};
        label_str(regexp(label_str,'[ .()]'))=[];
        
        % compute proximity based on labels / create animation
        output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
        label_file = [data_folder '/' subfolders{j} '/' label_files{j} ];
        if create_animations
            %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
        else
            %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        end
        
        % save data to h5 files
        h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
        if exist(h5_filename, 'file')==2
            delete(h5_filename);
        end
        h5create(h5_filename,'/Labels', size(labels));
        h5write(h5_filename,'/Labels', labels);
        
        h5create(h5_filename,'/Direction', size(direction));
        h5write(h5_filename,'/Direction', direction);
        
        h5create(h5_filename, '/Data',size(dataset));
        h5write(h5_filename, '/Data',dataset);
        disp(['Data written for view ' label_names{j}])
    end
end

%% Virtual Trajectories iFIND00113
clear
%
data_folder = 'D:\iFIND00113\';
for k=1:101
    subfolders = {['Abdomen\virtual_trajectory_' num2str(k-1)], ['Head\virtual_trajectory_' num2str(k-1)], ['Limbs\virtual_trajectory_' num2str(k-1)]};
    label_files = {'abdo_labels.csv', 'head_labels_es.csv', 'femur_labels_es.csv'};
    label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};
    
    
    % Store the index of the middle standard plane cluster to use
    Middle_Cluster{1}=2; % Abdomen
    Middle_Cluster{2}=1; % Head
    Middle_Cluster{3}=3; % Femur
    
    % options
    create_animations = true;
    image_size = [64 64]';%[255 255]';
    image_spacing = [2 2]';%[0.5 0.5]';
    %image_centre = [0 0]';
    
    
    for j=1%:numel(label_names)
        
        imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
        nfiles = length(imagefiles);    % Number of files found
        dataset = [];
        for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
            [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
            %currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size],'blur',[3 3],[0.5 0.5]);
            currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
            images_patient{j}(i) = currentimage; % Save the image
            dataset(:,:,i) = images_patient{j}(i).data;
            if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
                if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                    f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                    for k=1:numel(f)
                        info = rmfield(info, f{k});
                    end
                    
                else
                    f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                    for k=1:numel(f)
                        info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                    end
                    
                end
            end
            
            images_info_patient{j}(i) = info; % Store the information
        end
        label_str = label_names{j};
        label_str(regexp(label_str,'[ .()]'))=[];
        
        % compute proximity based on labels / create animation
        output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
        label_file = [data_folder '/' subfolders{j} '/' label_files{j} ];
        if create_animations
            %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
        else
            %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        end
        
        % save data to h5 files
        h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
        if exist(h5_filename, 'file')==2
            delete(h5_filename);
        end
        h5create(h5_filename,'/Labels', size(labels));
        h5write(h5_filename,'/Labels', labels);
        
        h5create(h5_filename,'/Direction', size(direction));
        h5write(h5_filename,'/Direction', direction);
        
        h5create(h5_filename, '/Data',size(dataset));
        h5write(h5_filename, '/Data',dataset);
        disp(['Data written for view ' label_names{j}])
    end
end

%% Virtual Trajectories iFIND00485
clear
%
data_folder = 'D:\iFIND00485\';
for k=1:101
    subfolders = {['Abdomen\virtual_trajectory_' num2str(k-1)], ['Head\virtual_trajectory_' num2str(k-1)], ['Limbs\virtual_trajectory_' num2str(k-1)]};
    label_files = {'abdo_labels.csv', 'head_labels_es.csv', 'femur_labels_es.csv'};
    label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};
    
    
    % Store the index of the middle standard plane cluster to use
    Middle_Cluster{1}=2; % Abdomen
    Middle_Cluster{2}=1; % Head
    Middle_Cluster{3}=3; % Femur
    
    % options
    create_animations = true;
    image_size = [64 64]';%[255 255]';
    image_spacing = [2 2]';%[0.5 0.5]';
    image_centre = [0 80]';
    
    
    for j=1%:numel(label_names)
        
        imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
        nfiles = length(imagefiles);    % Number of files found
        dataset = [];
        for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
            [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
 %          currentimage = resampleImage(currentimage, [],'spacing_and_size_and_centre',[image_spacing ; image_size; image_centre]);
            %currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size],'blur',[3 3],[0.5 0.5]);
            currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
            images_patient{j}(i) = currentimage; % Save the image
            dataset(:,:,i) = images_patient{j}(i).data;
            if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
                if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                    f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                    for k=1:numel(f)
                        info = rmfield(info, f{k});
                    end
                    
                else
                    f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                    for k=1:numel(f)
                        info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                    end
                    
                end
            end
            
            images_info_patient{j}(i) = info; % Store the information
        end
        label_str = label_names{j};
        label_str(regexp(label_str,'[ .()]'))=[];
        
        % compute proximity based on labels / create animation
        output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
        label_file = [data_folder '/' subfolders{j} '/' label_files{j} ];
        if create_animations
            %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
        else
            %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        end
        
        % save data to h5 files
        h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
        if exist(h5_filename, 'file')==2
            delete(h5_filename);
        end
        h5create(h5_filename,'/Labels', size(labels));
        h5write(h5_filename,'/Labels', labels);
        
        h5create(h5_filename,'/Direction', size(direction));
        h5write(h5_filename,'/Direction', direction);
        
        h5create(h5_filename, '/Data',size(dataset));
        h5write(h5_filename, '/Data',dataset);
        disp(['Data written for view ' label_names{j}])
    end
end



%% Virtual Trajectories iFIND00118
clear
%
data_folder = 'D:\iFIND00118\';
for k=1:101
    subfolders = {['Abdomen\virtual_trajectory_' num2str(k-1)], ['Head\virtual_trajectory_' num2str(k-1)], ['Limbs\virtual_trajectory_' num2str(k-1)]};
    label_files = {'abdo_labels.csv', 'head_labels_es.csv', 'femur_labels_es.csv'};
    label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};
    
    
    % Store the index of the middle standard plane cluster to use
    Middle_Cluster{1}=2; % Abdomen
    Middle_Cluster{2}=1; % Head
    Middle_Cluster{3}=3; % Femur
    
    % options
    create_animations = true;
    image_size = [64 64]';%[255 255]';
    image_spacing = [2 2]';%[0.5 0.5]';
    image_centre = [0 80]';
    
    
    for j=1%:numel(label_names)
        
        imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
        nfiles = length(imagefiles);    % Number of files found
        dataset = [];
        for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
            [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
 %          currentimage = resampleImage(currentimage, [],'spacing_and_size_and_centre',[image_spacing ; image_size; image_centre]);
            %currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size],'blur',[3 3],[0.5 0.5]);
            currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
            images_patient{j}(i) = currentimage; % Save the image
            dataset(:,:,i) = images_patient{j}(i).data;
            if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
                if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                    f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                    for k=1:numel(f)
                        info = rmfield(info, f{k});
                    end
                    
                else
                    f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                    for k=1:numel(f)
                        info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                    end
                    
                end
            end
            
            images_info_patient{j}(i) = info; % Store the information
        end
        label_str = label_names{j};
        label_str(regexp(label_str,'[ .()]'))=[];
        
        % compute proximity based on labels / create animation
        output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
        label_file = [data_folder '/' subfolders{j} '/' label_files{j} ];
        if create_animations
            %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
        else
            %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        end
        
        % save data to h5 files
        h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
        if exist(h5_filename, 'file')==2
            delete(h5_filename);
        end
        h5create(h5_filename,'/Labels', size(labels));
        h5write(h5_filename,'/Labels', labels);
        
        h5create(h5_filename,'/Direction', size(direction));
        h5write(h5_filename,'/Direction', direction);
        
        h5create(h5_filename, '/Data',size(dataset));
        h5write(h5_filename, '/Data',dataset);
        disp(['Data written for view ' label_names{j}])
    end
end


%% Virtual Trajectories iFIND00119
clear
%
data_folder = 'D:\iFIND00119\';
for k=1:101
    subfolders = {['Abdomen\virtual_trajectory_' num2str(k-1)], ['Head\virtual_trajectory_' num2str(k-1)], ['Limbs\virtual_trajectory_' num2str(k-1)]};
    label_files = {'abdo_labels.csv', 'head_labels_es.csv', 'femur_labels_es.csv'};
    label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};
    
    
    % Store the index of the middle standard plane cluster to use
    Middle_Cluster{1}=2; % Abdomen
    Middle_Cluster{2}=1; % Head
    Middle_Cluster{3}=3; % Femur
    
    % options
    create_animations = true;
    image_size = [64 64]';%[255 255]';
    image_spacing = [2 2]';%[0.5 0.5]';
    image_centre = [0 80]';
    
    
    for j=1%:numel(label_names)
        
        imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
        nfiles = length(imagefiles);    % Number of files found
        dataset = [];
        for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
            [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
 %          currentimage = resampleImage(currentimage, [],'spacing_and_size_and_centre',[image_spacing ; image_size; image_centre]);
            %currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size],'blur',[3 3],[0.5 0.5]);
            currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
            images_patient{j}(i) = currentimage; % Save the image
            dataset(:,:,i) = images_patient{j}(i).data;
            if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
                if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                    f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                    for k=1:numel(f)
                        info = rmfield(info, f{k});
                    end
                    
                else
                    f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                    for k=1:numel(f)
                        info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                    end
                    
                end
            end
            
            images_info_patient{j}(i) = info; % Store the information
        end
        label_str = label_names{j};
        label_str(regexp(label_str,'[ .()]'))=[];
        
        % compute proximity based on labels / create animation
        output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
        label_file = [data_folder '/' subfolders{j} '/' label_files{j} ];
        if create_animations
            %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
        else
            %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        end
        
        % save data to h5 files
        h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
        if exist(h5_filename, 'file')==2
            delete(h5_filename);
        end
        h5create(h5_filename,'/Labels', size(labels));
        h5write(h5_filename,'/Labels', labels);
        
        h5create(h5_filename,'/Direction', size(direction));
        h5write(h5_filename,'/Direction', direction);
        
        h5create(h5_filename, '/Data',size(dataset));
        h5write(h5_filename, '/Data',dataset);
        disp(['Data written for view ' label_names{j}])
    end
end

%% Virtual Trajectories iFIND00116
clear
%
data_folder = 'D:\iFIND00116\';
for k=1:101
    subfolders = {['Abdomen\virtual_trajectory_' num2str(k-1)], ['Head\virtual_trajectory_' num2str(k-1)], ['Limbs\virtual_trajectory_' num2str(k-1)]};
    label_files = {'abdo_labels.csv', 'head_labels_es.csv', 'femur_labels_es.csv'};
    label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};
    
    
    % Store the index of the middle standard plane cluster to use
    Middle_Cluster{1}=2; % Abdomen
    Middle_Cluster{2}=1; % Head
    Middle_Cluster{3}=3; % Femur
    
    % options
    create_animations = true;
    image_size = [64 64]';%[255 255]';
    image_spacing = [2 2]';%[0.5 0.5]';
    image_centre = [0 80]';
    
    
    for j=1%:numel(label_names)
        
        imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
        nfiles = length(imagefiles);    % Number of files found
        dataset = [];
        for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
            [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
 %          currentimage = resampleImage(currentimage, [],'spacing_and_size_and_centre',[image_spacing ; image_size; image_centre]);
            %currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size],'blur',[3 3],[0.5 0.5]);
            currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
            images_patient{j}(i) = currentimage; % Save the image
            dataset(:,:,i) = images_patient{j}(i).data;
            if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
                if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                    f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                    for k=1:numel(f)
                        info = rmfield(info, f{k});
                    end
                    
                else
                    f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                    for k=1:numel(f)
                        info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                    end
                    
                end
            end
            
            images_info_patient{j}(i) = info; % Store the information
        end
        label_str = label_names{j};
        label_str(regexp(label_str,'[ .()]'))=[];
        
        % compute proximity based on labels / create animation
        output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
        label_file = [data_folder '/' subfolders{j} '/' label_files{j} ];
        if create_animations
            %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
        else
            %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        end
        
        % save data to h5 files
        h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
        if exist(h5_filename, 'file')==2
            delete(h5_filename);
        end
        h5create(h5_filename,'/Labels', size(labels));
        h5write(h5_filename,'/Labels', labels);
        
        h5create(h5_filename,'/Direction', size(direction));
        h5write(h5_filename,'/Direction', direction);
        
        h5create(h5_filename, '/Data',size(dataset));
        h5write(h5_filename, '/Data',dataset);
        disp(['Data written for view ' label_names{j}])
    end
end


% Virtual Trajectories iFIND00121
clear
%
data_folder = 'D:\iFIND00121\';
for k=1:101
    subfolders = {['Abdomen\virtual_trajectory_' num2str(k-1)], ['Head\virtual_trajectory_' num2str(k-1)], ['Limbs\virtual_trajectory_' num2str(k-1)]};
    label_files = {'abdo_labels.csv', 'head_labels_es.csv', 'femur_labels_es.csv'};
    label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};
    
    
    % Store the index of the middle standard plane cluster to use
    Middle_Cluster{1}=2; % Abdomen
    Middle_Cluster{2}=1; % Head
    Middle_Cluster{3}=3; % Femur
    
    % options
    create_animations = true;
    image_size = [64 64]';%[255 255]';
    image_spacing = [2 2]';%[0.5 0.5]';
    image_centre = [0 80]';
    
    
    for j=1%:numel(label_names)
        
        imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
        nfiles = length(imagefiles);    % Number of files found
        dataset = [];
        for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
            [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
 %          currentimage = resampleImage(currentimage, [],'spacing_and_size_and_centre',[image_spacing ; image_size; image_centre]);
            %currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size],'blur',[3 3],[0.5 0.5]);
            currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
            images_patient{j}(i) = currentimage; % Save the image
            dataset(:,:,i) = images_patient{j}(i).data;
            if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
                if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                    f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                    for k=1:numel(f)
                        info = rmfield(info, f{k});
                    end
                    
                else
                    f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                    for k=1:numel(f)
                        info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                    end
                    
                end
            end
            
            images_info_patient{j}(i) = info; % Store the information
        end
        label_str = label_names{j};
        label_str(regexp(label_str,'[ .()]'))=[];
        
        % compute proximity based on labels / create animation
        output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
        label_file = [data_folder '/' subfolders{j} '/' label_files{j} ];
        if create_animations
            %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
        else
            %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        end
        
        % save data to h5 files
        h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
        if exist(h5_filename, 'file')==2
            delete(h5_filename);
        end
        h5create(h5_filename,'/Labels', size(labels));
        h5write(h5_filename,'/Labels', labels);
        
        h5create(h5_filename,'/Direction', size(direction));
        h5write(h5_filename,'/Direction', direction);
        
        h5create(h5_filename, '/Data',size(dataset));
        h5write(h5_filename, '/Data',dataset);
        disp(['Data written for view ' label_names{j}])
    end
end


% Virtual Trajectories iFIND00122
clear
%
data_folder = 'D:\iFIND00122\';
for k=1:101
    subfolders = {['Abdomen\virtual_trajectory_' num2str(k-1)], ['Head\virtual_trajectory_' num2str(k-1)], ['Limbs\virtual_trajectory_' num2str(k-1)]};
    label_files = {'abdo_labels.csv', 'head_labels_es.csv', 'femur_labels_es.csv'};
    label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};
    
    
    % Store the index of the middle standard plane cluster to use
    Middle_Cluster{1}=2; % Abdomen
    Middle_Cluster{2}=1; % Head
    Middle_Cluster{3}=3; % Femur
    
    % options
    create_animations = true;
    image_size = [64 64]';%[255 255]';
    image_spacing = [2 2]';%[0.5 0.5]';
    image_centre = [0 80]';
    
    
    for j=1%:numel(label_names)
        
        imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
        nfiles = length(imagefiles);    % Number of files found
        dataset = [];
        for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
            [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
 %          currentimage = resampleImage(currentimage, [],'spacing_and_size_and_centre',[image_spacing ; image_size; image_centre]);
            %currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size],'blur',[3 3],[0.5 0.5]);
            currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
            images_patient{j}(i) = currentimage; % Save the image
            dataset(:,:,i) = images_patient{j}(i).data;
            if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
                if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                    f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                    for k=1:numel(f)
                        info = rmfield(info, f{k});
                    end
                    
                else
                    f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                    for k=1:numel(f)
                        info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                    end
                    
                end
            end
            
            images_info_patient{j}(i) = info; % Store the information
        end
        label_str = label_names{j};
        label_str(regexp(label_str,'[ .()]'))=[];
        
        % compute proximity based on labels / create animation
        output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
        label_file = [data_folder '/' subfolders{j} '/' label_files{j} ];
        if create_animations
            %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
        else
            %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        end
        
        % save data to h5 files
        h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
        if exist(h5_filename, 'file')==2
            delete(h5_filename);
        end
        h5create(h5_filename,'/Labels', size(labels));
        h5write(h5_filename,'/Labels', labels);
        
        h5create(h5_filename,'/Direction', size(direction));
        h5write(h5_filename,'/Direction', direction);
        
        h5create(h5_filename, '/Data',size(dataset));
        h5write(h5_filename, '/Data',dataset);
        disp(['Data written for view ' label_names{j}])
    end
end


% Virtual Trajectories iFIND00123
clear
%
data_folder = 'D:\iFIND00123\';
for k=1:101
    subfolders = {['Abdomen\virtual_trajectory_' num2str(k-1)], ['Head\virtual_trajectory_' num2str(k-1)], ['Limbs\virtual_trajectory_' num2str(k-1)]};
    label_files = {'abdo_labels.csv', 'head_labels_es.csv', 'femur_labels_es.csv'};
    label_names = {'Abdomen', 'Brain (Tv.)', 'Femur'};
    
    
    % Store the index of the middle standard plane cluster to use
    Middle_Cluster{1}=2; % Abdomen
    Middle_Cluster{2}=1; % Head
    Middle_Cluster{3}=3; % Femur
    
    % options
    create_animations = true;
    image_size = [64 64]';%[255 255]';
    image_spacing = [2 2]';%[0.5 0.5]';
    image_centre = [0 80]';
    
    
    for j=1%:numel(label_names)
        
        imagefiles = dir([data_folder '/' subfolders{j} '/**/*.mhd']); % all .mhd files in the (sub)folder
        nfiles = length(imagefiles);    % Number of files found
        dataset = [];
        for i=nfiles:-1:1  % going backwards makes pre-allocation of the array automatically
            [currentimage, info] = read_mhd([imagefiles(i).folder '/' imagefiles(i).name]);
 %          currentimage = resampleImage(currentimage, [],'spacing_and_size_and_centre',[image_spacing ; image_size; image_centre]);
            %currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size],'blur',[3 3],[0.5 0.5]);
            currentimage = resampleImage(currentimage, [],'spacing_and_size',[image_spacing ; image_size]);
            images_patient{j}(i) = currentimage; % Save the image
            dataset(:,:,i) = images_patient{j}(i).data;
            if i<nfiles && ( numel(fieldnames(info)) ~= numel(fieldnames((images_info_patient{j}(i+1)))))
                if  numel(fieldnames((images_info_patient{j}(i+1)))) < numel(fieldnames(info))
                    f = setdiff(fieldnames(info), fieldnames(images_info_patient{j}(i+1)));
                    for k=1:numel(f)
                        info = rmfield(info, f{k});
                    end
                    
                else
                    f = setdiff(fieldnames(images_info_patient{j}(i+1)), fieldnames(info));
                    for k=1:numel(f)
                        info.(f{k}) = images_info_patient{j}(i+1).(f{k});
                    end
                    
                end
            end
            
            images_info_patient{j}(i) = info; % Store the information
        end
        label_str = label_names{j};
        label_str(regexp(label_str,'[ .()]'))=[];
        
        % compute proximity based on labels / create animation
        output_gif_filename = [data_folder '/' subfolders{j} '/' label_files{1}(1:end-4) '_' label_str '.gif'];
        label_file = [data_folder '/' subfolders{j} '/' label_files{j} ];
        if create_animations
            %[labels, proximity] = compute_proximity_values(images_patient,images_info_patient, label_file, label_names{j}, output_gif_filename);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j}, output_gif_filename);
        else
            %[labels, proximity] = compute_proximity_values(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
            [labels, direction] = compute_direction_values_virtual_guidance(images_patient{j},images_info_patient{j}, label_file, label_names{j},[]);
        end
        
        % save data to h5 files
        h5_filename = [data_folder '/' subfolders{1} '/' label_files{1}(1:end-4) '_' label_str '.h5'];
        if exist(h5_filename, 'file')==2
            delete(h5_filename);
        end
        h5create(h5_filename,'/Labels', size(labels));
        h5write(h5_filename,'/Labels', labels);
        
        h5create(h5_filename,'/Direction', size(direction));
        h5write(h5_filename,'/Direction', direction);
        
        h5create(h5_filename, '/Data',size(dataset));
        h5write(h5_filename, '/Data',dataset);
        disp(['Data written for view ' label_names{j}])
    end
end